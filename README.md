# audiogame-manager

Installer and launcher for Windows based audio games under wine for Linux.

## Usage

    ./audiogame-manager.sh -c

Check your system for components needed by the audiogame manager program.

    ./audiogame-manager.sh -h

Get usage instructions.

    ./audiogame-manager.sh -i

Select from a list of games to install.

With no arguments, the list of installed games will be displayed. In all the menus, both installer and launcher, use the up and down arrow keys to find the game you want. Press enter on the game to perform the action. For more detailed help, try the -h flag.


## Adding Games

Because of the size of the audiogame-manager.sh file, adding new titles may seem like a daunting task. For most games, however, the process is not terribly difficult. The majority of them require two edits.


### The Menu Section

You need to add the game title as it appears in the menu when you launch audiogame-manager.sh. Let's use the fictitious title "My Awesome Game" in the following example.

First, find the list of games. This is easily done by searching for the comment:

    # The list of games available for installation.

You will notice an array called gameList. Add the title, in quotes, to the array. Use the existing list as a guide to keep things formatted similarly. Games are approximately alphebatized, although I did add some of them prier to coffee, so that doesn't always hold true. So for our example, the entry here would be:

    "My Awesome Game"

That's it for the menu. It's worth noting that to disable a game from showing up in the installation menu, just add a # to comment out the entry.

### Installing the Game

In most cases, the process to install a game is straight forward. If the game in question is self-voiced, it is very easy in deed. The only difficult part may be finding out what, if any wine packages are needed. Continuing with the example game, let's say for the time being that it is a self-voiced game that is 32 bit and will work on any version of wine, so we don't care about setting it.

Once again, you can search for a string to get you to the top of the installation section. As before, the installers are approximately alphabetically ordered. So moving down to the "M" section, let's add the installer. find the string:

    # Install game based on the selection above.

Then in the appropriate area, add the installer code. The example will have comments explaining what's going on.

        # Add the game title to the case statement
    "My Awesome Game")
            # Set up the wine prefix in ~/.local/wine/my-awesome-game
            install_wine_bottle
            # Get the game's installation file
            download "http://.example.com/my_awesome_game_setup.exe"
            # run the installer with the silent flag so that it installs without any prompts which are inaccessible to screen readers on Linux.
            wine "${cache}/my_awesome_game_setup.exe" /silent
            # Create the launcher entry so audiogame manager knows how to start the game.
            add_launcher "c:\Program Files\My Awesome Game\mag.exe"
    ;;


### Dealing with nvdaControllerClient.dll and SAPI

Note that currently the 64 bit wine version of SAPI does not work. Games that include the nvdaControllerClient will work, but in order for wine to process the speech with SAPI, the dll file needs to be removed. Let's change the above example because the imaginary developer of My Awesome Game got tired of recording all of those voice clips and changed the game to use SAPI instead. Here is the updated code with comments.

        # Add the game title to the case statement
    "My Awesome Game")
            # Set up the wine prefix in ~/.local/wine/my-awesome-game and install speechsdk
            install_wine_bottle speechsdk
            # Get the game's installation file
            download "http://.example.com/my_awesome_game_setup.exe"
            # run the installer with the silent flag so that it installs without any prompts which are inaccessible to screen readers on Linux.
            wine "${cache}/my_awesome_game_setup.exe" /silent
            # Remove the nvdaControllerClient.dll file
            # Note that it may just becalled nvdaControllerClient.dll or nvdaControllerClient32.dll
            find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
            # Create the launcher entry so audiogame manager knows how to start the game.
            add_launcher "c:\Program Files\My Awesome Game\mag.exe"
    ;;

In some cases, however, this will cause the game to not function at all. For these cases, we have a fake dll that can take the place of the original. You just need to make sure the fake dll is downloaded during game installation and then move it into place. For an example of how this works, please take a look at the "A Hero's Call" entry in audiogame-manager.sh.

### Dealing with compressed files

This is actually pretty easy. Just change the wine command to the command to extract the file to the proper location. If the "My Awesome Game" developer got tired of dealing with setup files and switched to zip, for instance, the new line would look like this.

    unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/my_awesome_game.zip"

Some games just extract to the current directory, which can make "Program Files" all cluttered. If you find this is the case, just add a directory name on to the end and it will be created, like so.

    unzip -d "$WINEPREFIX/drive_c/Program Files/My Awesome Game" "${cache}/my_awesome_game.zip"


### When Things Get Complicated

If your game requires some extra setup before launching, there is a section to add code for it. As usual, there is a comment at the start of the section. Look for the string.

    # for games that require custom scripts before launch or custom launch parameters

There are some tools available for getting games to speak that do not use any of the methods listed above. Look in the speech directory and you will find a clipboard translator and a script to read the current window title when it changes.

If your game is 64 bit only, you will need to add a section here to launch it with the nvda2speechd setup. For a detailed example of this, check out the "Code Dungeon" installer. This is a good one too because it demonstrates an oddity required to get the keyboard working as well as setting the Windows version and architecture. It also shows how to prompt the user for a file that cannot be downloaded automatically, e.g. something from Itch.

That covers most things. If the game happens to require .net, it can really get complicated indeed. For an example of this, take a look at the "Entombed" installer.


## Contributing

If you would like to contribute, please try to keep the code formatted like it is currently. Please make sure that your additions work on your computer before doing a pull request.
