#
#    Ã contents of this file are subject to the Common Public Attribution
#    License Version 1.0 (the ÃenseÃyou may not use this file except in
#    compliance with the License. You may obtain a copy of the License at
#    https://opensource.org/licenses/CPAL-1.0. The License is based on the Mozilla Public License Version
#    1.1 but Sections 14 and 15 have been added to cover use of software over a
#    computer network and provide for limited attribution for the Original
#    Developer. In addition, Exhibit A has been modified to be consistent with
#    Exhibit B.
#
#    Software distributed under the License is distributed on an ÃISÃsis,
#    WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#    for the specific language governing rights and limitations under the
#    License.
#
#    The Original Code is audiogame manager.
#
#    The Original Developer is not the Initial Developer and is . If
#    left blank, the Original Developer is the Initial Developer.
#
#    The Initial Developer of the Original Code is Billy "Storm Dragon" Wolfe. All portions of
#    the code written by Billy Wolfe are Copyright (c) 2020. All Rights
#    Reserved.
#
#    Contributor Michael Taboada.
#
#    Attribution Copyright Notice: Audiogame manager copyright 2020 Storm Dragon. All rights reserved.
#
#    Attribution Phrase (not exceeding 10 words): A Stormux project
#
#    Attribution URL: https://stormgames.wolfe.casa
#
#    Graphic Image as provided in the Covered Code, if any.
#
#    Display of Attribution Information is required in Larger
#    Works which are defined in the CPAL as a work which combines Covered Code
#    or portions thereof with code not governed by the terms of the CPAL.
 
# Allows you to select a wine prefix to run with audiogame manager
#Get Working Directory
tell application "Finder"
	set currentPath to container of (path to me) as alias
end tell
#format Converters
on convertPathToAlias(thePath)
	tell application "System Events"
		try
			return (path of disk item (thePath as string)) as alias
		on error
			return (path of disk item (path of thePath) as string) as alias
		end try
	end tell
end convertPathToAlias

on convertPathToPOSIXString(thePath)
	tell application "System Events"
		try
			set thePath to path of disk item (thePath as string)
		on error
			set thePath to path of thePath
		end try
	end tell
	return POSIX path of thePath
end convertPathToPOSIXString

set currentPath to convertPathToPOSIXString(currentPath)
set folderPath to POSIX file "~/.local/wine/"
set folderPath to convertPathToAlias(folderPath)
set gameFolder to choose folder with prompt "Choose your game and press enter:" default location folderPath
set gameFolder to convertPathToPOSIXString(gameFolder)
set gameFolder to quoted form of gameFolder
set gameName to do shell script ("basename " & gameFolder)
do shell script "/usr/local/bin/bash -l " & currentPath & "../audiogame-manager.sh -l " & gameName & " &> /dev/null &"
do shell script "say -v alex Launching " & gameName & ", please wait..."
