#!/usr/bin/env bash
# This script installs Cepstral voices for use with the audiogame-manager
# To purchase a license for a voice you like, visit https://cepstral.com

# This script is released under the terms of the WTFPL: http://wtfpl.net
# Installer files are saved to ~/Downloads


# Dialog accessibility
export DIALOGOPTS='--no-lines --visit-items'
# Turn off debug messages
export WINEDEBUG="-all"
# Make sure display is set in case installed from console.
export DISPLAY="${DISPLAY:-:0}"
# Version number, to easily keep script up-to-date.
version="6.2.3.801"
# Msi or exe? Some are different.
msiexe="exe"
# Beginning part of url, for making things easier.
prefix="https://www.cepstral.com/downloads/installers/windows/"

register() {
    local v="$1"
    company="$(dialog --clear --inputbox "Company name (leave empty if none)" -1 -1 --stdout)" || exit $?
    customer="$(dialog --clear --inputbox "Customer name" -1 -1 --stdout)" || exit $?
    key="$(dialog --clear --inputbox "License key" -1 -1 --stdout)" || exit $?
    eval "wine \"c:\\Program Files\\Cepstral\\bin\\swift.exe\" --reg-voice --voice-name \"$v\" --customer-name \"$customer\" --company-name \"$company\" --license-key \"$key\""
    exit 0
}


action="install"
declare -a voices=(
    "Allison"
    "Allison"
    "Amy"
    "Amy"
    "Belle"
    "Belle"
    "Callie"
    "Callie"
    "Charlie"
    "Charlie"
    "Dallas"
    "Dallas"
    "Damien"
    "Damien"
    "David"
    "David"
    "Diane"
    "Diane"
    "Duchess"
    "Duchess"
    "Duncan"
    "Duncan"
    "Emily"
    "Emily"
    "Linda"
    "Linda"
    "Robin"
    "Robin"
    "Shouty"
    "Shouty"
    "Walter"
    "Walter"
    "Whispery"
    "Whispery"
    "William"
    "William"
)

# Get the desired wine bottle
                                                                                                                                                      
declare -a bottle
for i in $(find ~/.local/wine/ -maxdepth 1 -type d -not -name 'wine' | sort) ; do
    bottle+=("$i" "${i##*/}")
    done
export WINEPREFIX="$(dialog --backtitle "Use the up and down arrow keys to find the option you want, then press enter to select it." \
    --clear \
        --no-tags \
        --menu "Select A Wine Bottle" 0 0 0 "${bottle[@]}" --stdout)"
                                                                                                                                                      
                                                                                                                                                      
if [[ -z "${WINEPREFIX}" ]]; then
    exit 0
fi
 

if [[ "$1" == "-r" || "$1" == "--register" ]]; then
    action="register"
fi

voice="$(dialog --backtitle "Use the up and down arrow keys to find the option you want, then press enter to select it." \
    --clear \
        --no-tags \
        --menu "Select A voice to $action" 0 0 0 "${voices[@]}" --stdout)"
                                                                                                                                                      
if [[ "$action" == "register" ]]; then
    register $voice
fi

# make sure the ~/Downloads directory exists.
mkdir -p ~/Downloads

if ! [[ -e ~/Downloads/Cepstral_${voice}_windows_${version}.${msiexe} ]]; then
    wget -P ~/Downloads/ "${prefix}Cepstral_${voice}_windows_${version}.${msiexe}" | dialog --progressbox "Downloading voice..." -1 -1
fi

# Get the install command.
cmd="wine"
[ "$msiexe" = "msi" ] && cmd="${cmd} msiexec /i"
cmd="${cmd} ~/Downloads/Cepstral_${voice}_windows_${version}.${msiexe}"
[ "$msiexe" = "msi" ] && cmd="${cmd} /q"
# Install the voice
(eval "$cmd" &
[ "$msiexe" = "exe" ] && xdotool sleep 20 key --delay 75 alt+n key --delay 75 alt+a key --delay 75 alt+n key --delay 75 alt+o key --delay 75 alt+i sleep 20 key --delay 75 alt+f
wineserver -w) | dialog --progressbox "installing voice..." -1 -1

# Make voices louder.
find "${WINEPREFIX}/drive_c/Program Files/Cepstral/voices" -type d -not -name voices -exec bash -c 'for d ; do echo "GAIN 2.5" > "$d/default.sfx";done' _ {} \;

exit 0
