#!/bin/bash
# Adapted from the bash snippet found at:
# https://bbs.archlinux.org/viewtopic.php?id=117031
                                                                                                                                                                          
# Wait for the application to start
while ! pgrep -u "$USER" ^$1 &> /dev/null ; do
    sleep 0.05
done

# Read so long as the application is running
while pgrep -u "$USER" ^$1 &> /dev/null ; do    
    wnd_focus=$(xdotool getwindowfocus)
    wnd_title=$(xprop -id $wnd_focus WM_NAME)
    lookfor='"(.*)"'
                                                                                                                                                                          
    if [[ "$wnd_title" =~ $lookfor ]]; then
        wnd_title=${BASH_REMATCH[1]}
        if [[ "$old_title" != "$wnd_title" ]]; then
            spd-say -- "$wnd_title"
            old_title="$wnd_title"
        fi
    fi
    sleep 0.01
done
