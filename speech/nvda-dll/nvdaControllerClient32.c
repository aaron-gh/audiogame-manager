#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
unsigned long __stdcall __declspec(dllexport) nvdaController_speakText(const wchar_t* text);
unsigned long __stdcall __declspec(dllexport) nvdaController_cancelSpeech();
unsigned long __stdcall __declspec(dllexport) nvdaController_brailleMessage(const wchar_t* text);
unsigned long __stdcall __declspec(dllexport) nvdaController_testIfRunning();
#ifdef __cplusplus
}
#endif

unsigned long __stdcall __declspec(dllexport) nvdaController_speakText(const wchar_t* text) {
	return 420;
}

unsigned long __stdcall __declspec(dllexport) nvdaController_cancelSpeech() {
	return 420;
}

unsigned long __stdcall __declspec(dllexport) nvdaController_brailleMessage(const wchar_t* text) {
	return 420;
}

unsigned long __stdcall __declspec(dllexport) nvdaController_testIfRunning() {
	return 420;
}
