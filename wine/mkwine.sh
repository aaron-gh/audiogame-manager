#!/bin/bash
#
#    âe contents of this file are subject to the Common Public Attribution
#    License Version 1.0 (the âcenseâ you may not use this file except in
#    compliance with the License. You may obtain a copy of the License at
#    https://opensource.org/licenses/CPAL-1.0. The License is based on the Mozilla Public License Version
#    1.1 but Sections 14 and 15 have been added to cover use of software over a
#    computer network and provide for limited attribution for the Original
#    Developer. In addition, Exhibit A has been modified to be consistent with
#    Exhibit B.
#
#    Software distributed under the License is distributed on an â ISâasis,
#    WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#    for the specific language governing rights and limitations under the
#    License.
#
#    The Original Code is audiogame manager.
#
#    The Original Developer is not the Initial Developer and is . If
#    left blank, the Original Developer is the Initial Developer.
#
#    The Initial Developer of the Original Code is Billy "Storm Dragon" Wolfe. All portions of
#    the code written by Billy Wolfe are Copyright (c) 2020. All Rights
#    Reserved.
#
#    Contributor Michael Taboada.
#
#    Attribution Copyright Notice: Audiogame manager copyright 2020 Storm Dragon. All rights reserved.
#
#    Attribution Phrase (not exceeding 10 words): A Stormux project
#
#    Attribution URL: https://stormgames.wolfe.casa
#
#    Graphic Image as provided in the Covered Code, if any.
#
#    Display of Attribution Information is required in Larger
#    Works which are defined in the CPAL as a work which combines Covered Code
#    or portions thereof with code not governed by the terms of the CPAL.
 

export WINEARCH=win32
bottle="${1,,}"
shift
bottle="${bottle//[[:space:]]/-}"
mkdir -p "$HOME/.local/wine/${bottle}"
export WINEPREFIX="$HOME/.local/wine/${bottle}"
# Arguments to the function are dependancies to be installed.
(wine msiexec /i z:/usr/share/wine/mono/$(ls -1 /usr/share/wine/mono/) /silent
    wine msiexec /i z:$(ls -1 /usr/share/wine/gecko/*x86.msi) /silent
    winetricks -q $@ ${winVer:-winxp} ${winetricksSettings}) | dialog --progressbox "Installing wine bottle, please wait..." -1 -1

exit 0
