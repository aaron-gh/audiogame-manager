#!/bin/bash
#
#    âe contents of this file are subject to the Common Public Attribution
#    License Version 1.0 (the âcenseâ you may not use this file except in
#    compliance with the License. You may obtain a copy of the License at
#    https://opensource.org/licenses/CPAL-1.0. The License is based on the Mozilla Public License Version
#    1.1 but Sections 14 and 15 have been added to cover use of software over a
#    computer network and provide for limited attribution for the Original
#    Developer. In addition, Exhibit A has been modified to be consistent with
#    Exhibit B.
#
#    Software distributed under the License is distributed on an â ISâasis,
#    WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#    for THE SPecific language governing rights and limitations under the
#    License.
#
#    The Original Code is audiogame manager.
#
#    The Original Developer is not the Initial Developer and is . If
#    left blank, the Original Developer is the Initial Developer.
#
#    The Initial Developer of the Original Code is Billy "Storm Dragon" Wolfe. All portions of
#    the code written by Billy Wolfe are Copyright (c) 2020. All Rights
#    Reserved.
#
#    Contributor Michael Taboada.
#
#    Attribution Copyright Notice: Audiogame manager copyright 2020 Storm Dragon. All rights reserved.
#
#    Attribution Phrase (not exceeding 10 words): A Stormux project
#
#    Attribution URL: https://stormgames.wolfe.casa
#
#    Graphic Image as provided in the Covered Code, if any.
#
#    Display of Attribution Information is required in Larger
#    Works which are defined in the CPAL as a work which combines Covered Code
#    or portions thereof with code not governed by the terms of the CPAL.
 
cache="${XDG_CACHE_HOME:-$HOME/.cache}/audiogame-manager"
updateURL="https://www.kaldobsky.com/audiogames"
updateFiles=("SwampPatch.zip")


dialog --backtitle "Audiogame manager" --yesno "If you do not have a full 32 bit gstreamer installation, the Swamp music can cause stuttering and crashes. Would you like to remove the music directory after installation?" -1 -1 --stdout
deleteMusic=$?

# Back up configuration files.
for i in losers.txt muted.txt scriptkeys.txt keyconfig.ini ; do
    cp -v ~/".local/wine/aprone/drive_c/Program Files/swamp/${i}" ~/".local/wine/aprone/drive_c/Program Files/swamp/${i}.agm"
    echo "${i} backed up as ${i}.agm"
done | dialog --progressbox "Backing up configuration files. They can be found in your swamp directory." -1 -1

sleep 3

# Download and extract updates
for i in "${updateFiles[@]}" ; do
    wget -O "${cache}/$i" "${updateURL}/$i"
    unzip -o -d ~/".local/wine/aprone/drive_c/Program Files/swamp" "${cache}/${i}"
done | dialog --progressbox "Updating Swamp, please wait..." -1 -1

        # Delete music if requested.
        if [[ $deleteMusic -eq 0 ]]; then
            rm -frv ~/".local/wine/aprone/drive_c/Program Files/swamp/sounds/Music/"
        fi


exit 0
