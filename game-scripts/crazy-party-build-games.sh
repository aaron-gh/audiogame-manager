#!/bin/bash
#
#    âe contents of this file are subject to the Common Public Attribution
#    License Version 1.0 (the âcenseâ you may not use this file except in
#    compliance with the License. You may obtain a copy of the License at
#    https://opensource.org/licenses/CPAL-1.0. The License is based on the Mozilla Public License Version
#    1.1 but Sections 14 and 15 have been added to cover use of software over a
#    computer network and provide for limited attribution for the Original
#    Developer. In addition, Exhibit A has been modified to be consistent with
#    Exhibit B.
#
#    Software distributed under the License is distributed on an â ISâasis,
#    WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#    for the specific language governing rights and limitations under the
#    License.
#
#    The Original Code is audiogame manager.
#
#    The Original Developer is not the Initial Developer and is . If
#    left blank, the Original Developer is the Initial Developer.
#
#    The Initial Developer of the Original Code is Billy "Storm Dragon" Wolfe. All portions of
#    the code written by Billy Wolfe are Copyright (c) 2020. All Rights
#    Reserved.
#
#    Contributor Michael Taboada.
#
#    Attribution Copyright Notice: Audiogame manager copyright 2020 Storm Dragon. All rights reserved.
#
#    Attribution Phrase (not exceeding 10 words): A Stormux project
#
#    Attribution URL: https://stormgames.wolfe.casa
#
#    Graphic Image as provided in the Covered Code, if any.
#
#    Display of Attribution Information is required in Larger
#    Works which are defined in the CPAL as a work which combines Covered Code
#    or portions thereof with code not governed by the terms of the CPAL.
 
for i in dialog unix2dos; do
    if ! command -v $i &> /dev/null ; then
        echo "Please install dialog and dos2unix before using this script."
    fi
done

if [[ $# -ne 1 ]]; then
    echo "usage: $0 filename without the .txt extension."
    exit 1
fi

mapfile -t gameList < <(tail +3 "$HOME/.local/wine/crazy-party/drive_c/Program Files/Crazy-Party-beta73/Mini-games reference.txt")

for i in "${gameList[@]}" ; do
    menuList+=("$i" "${i#* }" "off")
done

unset gameList
gameList="$(dialog --clear \
    --no-tags \
    --ok-label "Add Games" \
    --separate-output \
    --backtitle "Select games to add to the $1 list." \
    --checklist "Press space to check or uncheck a selected game." 0 0 0 "${menuList[@]}" --stdout)"

if [[ -z "${gameList}" ]]; then
    exit 0
fi

mkdir -p "$HOME/.local/wine/crazy-party/drive_c/Program Files/Crazy-Party-beta73/game"
echo "$gameList" >> "$HOME/.local/wine/crazy-party/drive_c/Program Files/Crazy-Party-beta73/game/${1}.txt"
sort -uno "$HOME/.local/wine/crazy-party/drive_c/Program Files/Crazy-Party-beta73/game/${1}.txt" "$HOME/.local/wine/crazy-party/drive_c/Program Files/Crazy-Party-beta73/game/${1}.txt"
eunix2dos "$HOME/.local/wine/crazy-party/drive_c/Program Files/Crazy-Party-beta73/game/${1}.txt"
dialog --infobox "Game list \"$1\" updated." 10 80
exit 0
