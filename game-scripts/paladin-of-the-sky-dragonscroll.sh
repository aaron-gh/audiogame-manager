#!/bin/bash
usage="
To use this script, call $0 with the name of the dragon scroll you want.
For example, $0 aura_master will use the aura_master dragon scroll.
Spaces should be replaced with the _ character.
Make sure that the dragon scroll you want to use is selected in the menu in the game before calling this script.
For easier use, you can bind a shortcut key to call the script.
For example, I have the latest scroll unlocked by Ross set to control+1.
"

export DISPLAY=:0

aura_master() {
    xdotool key --delay 75 return sleep .75 key --delay 75 alt sleep .5 key --delay 75 alt
}

avenging_spirit() {
    xdotool key --delay 75 return sleep .75 key --delay 75 alt $(for i in {1..7} ; do echo -n "sleep .385 key --delay 75 alt ";done) sleep .6 key --delay 75 alt sleep .7 key --delay 75 alt
}

black_hole() {
    xdotool key --delay 75 return sleep .8 key --delay 75 alt  $(for i in {1..3} ; do echo -n "sleep .74 key --delay 75 alt ";done) $(for i in {1..4} ; do echo -n "sleep .4 key --delay 75 alt ";done) sleep .6 key --delay 75 alt
}

charge_of_fury() {
    xdotool key --delay 75 return sleep .9 key --delay 75 alt sleep .4 key --delay 75 alt sleep .65 key --delay 75 alt sleep .6 key --delay 75 alt sleep .6 key --delay 75 alt
}

death_in_three_easy_steps() {
    xdotool key --delay 75 return sleep 1.5 key --delay 75 alt sleep 2.2 key --delay 75 alt sleep 3.2 key --delay 75 alt
}

demons_gift() {
    xdotool key --delay 75 return sleep .8 key --delay 75 alt sleep .4 key --delay 75 alt sleep 3.2 key --delay 75 alt
}

destruction_of_the_heavens() {
    local scrollString=("key --delay 75 return sleep .895 key --delay 75 alt")
    for i in {1..14} ; do
        scrollString+=("sleep .365 key --delay 75 alt")
    done
    scrollString+=("sleep .795 key --delay 75 alt")
    scrollString+=("sleep 2.5 key --delay 75 alt")
    xdotool ${scrollString[*]}
}

diabolical_fire() {
    xdotool key --delay 75 return sleep 1.2 key --delay 75 alt $(for i in {1..5} ; do echo -n "sleep .7 key --delay 75 alt ";done)
}

embodiment_of_cruelclaw() {
    xdotool key --delay 75 return sleep 1 key --delay 75 alt sleep .6 key --delay 75 alt sleep .8 key --delay 75 alt $(for i in {1..4} ; do echo -n "sleep .39 key --delay 75 alt ";done) sleep 1 key --delay 75 alt sleep 1.2 key --delay 75 alt sleep 1.2 key --delay 75 alt sleep 2.5 key --delay 75 alt
}

embodiment_of_power() {
    local scrollString=("key --delay 75 return")
    for i in {5..0..-3} ; do
        scrollString+=("sleep 1.${i} key --delay 75 alt")
    done
    for i in {1..2} ; do
        scrollString+=("sleep 1 key --delay 75 alt")
    done
    for i in {1..2} ; do
        scrollString+=("sleep 0.9 key --delay 75 alt")
    done
    for i in {8..4..-1} ; do
        scrollString+=("sleep 0.${i} key --delay 75 alt")
    done
    scrollString+=("sleep 0.4 key --delay 75 alt")
    xdotool ${scrollString[*]}
}

embodiment_of_righteousness() {
    xdotool key --delay 75 return sleep 3 key --delay 75 alt sleep 2.7 key --delay 75 alt sleep 2.7 key --delay 75 alt sleep .3 key --delay 75 alt sleep 1.1 key --delay 75 alt
}

embodiment_of_selflessness() {
    xdotool key --delay 75 return sleep .85 key --delay 75 alt sleep .4 key --delay 75 alt sleep 1.6 key --delay 75 alt sleep .4 key --delay 75 alt sleep .6 key --delay 75 alt
}

embodiment_of_valor() {
    xdotool key --delay 75 return sleep 1 key --delay 75 alt sleep .65 $(for i in {1..6} ; do echo "key --delay 75 alt sleep .415 ";done) sleep .7 key --delay 75 alt sleep 2.65 key --delay 75 alt
}

energy_burst() {
    xdotool key --delay 75 return sleep 1.22 key --delay 75 alt sleep .65 key --delay 75 alt sleep .4 key --delay 75 alt
}

fatal_blast() {
    xdotool key --delay 75 return sleep 1.5 key --delay 75 alt sleep .5 key --delay 75 alt sleep .7 key --delay 75 alt sleep .9 key --delay 75 alt sleep .6 key --delay 75 alt sleep .92 key --delay 75 alt sleep .7 key --delay 75 alt sleep 1 key --delay 75 alt sleep 1.2 key --delay 75 alt
}

flame_blizzard() {
    xdotool key --delay 75 return sleep 1 key --delay 75 alt sleep .5 key --delay 75 alt sleep .6 key --delay 75 alt sleep .6 key --delay 75 alt sleep .5 key --delay 75 alt sleep .4 key --delay 75 alt sleep .5 key --delay 75 alt
}

g_aura_master() {
    xdotool key --delay 75 return sleep .9 key --delay 75 alt sleep .5 key --delay 75 alt
}

g_charge_of_fury() {
    xdotool key --delay 75 return sleep .95 key --delay 75 alt sleep .4 key --delay 75 alt sleep .65 key --delay 75 alt sleep .6 key --delay 75 alt sleep .6 key --delay 75 alt
}

giga_candela() {
    xdotool key --delay 75 return sleep 2 key --delay 75 alt $(for i in {1..2} ; do echo "sleep .4 key --delay 75 alt ";done) sleep 1.3 key --delay 75 alt sleep 1 key --delay 75 alt sleep .7 key --delay 75 alt sleep .7 key --delay 75 alt sleep .5 key --delay 75 alt
}

gods_anger() {
    xdotool key --delay 75 return sleep .85 key --delay 75 alt sleep .3 key --delay 75 alt sleep .65 key --delay 75 alt sleep .4 key --delay 75 alt sleep .6 key --delay 75 alt sleep .6 key --delay 75 alt sleep .65 key --delay 75 alt
}

heavens_winds() {
    xdotool key --delay 75 return sleep .65 key --delay 75 alt $(for i in {1..7} ; do echo -n "sleep .32 key --delay 75 alt ";done)
}

infinite_pain() {
    xdotool key --delay 75 return sleep .8 key --delay 75 alt $(for i in {1..2} ; do echo "sleep .49 key --delay 75 alt ";done) sleep 3 key --delay 75 alt
}

infinite_pulse() {
    xdotool key --delay 75 return sleep .8 key --delay 75 alt $(for i in {1..3} ; do echo "sleep .4 key --delay 75 alt ";done) sleep .6 key --delay 75 alt sleep 1 key --delay 75 alt sleep 1.3 key --delay 75 alt sleep .4 key --delay 75 alt
}

lunar_cannon() {
    xdotool key --delay 75 return sleep .8 key --delay 75 alt sleep .7 key --delay 75 alt sleep 1.95 key --delay 75 alt sleep .7 key --delay 75 alt sleep .6 key --delay 75 alt
}

merciless_vengeance() {
# xdotool key --delay 250 return key --delay 250 Down key --delay 250 return $(for i in {1..4} ; do echo -n "key --delay 150 Down ";done)
    xdotool key --delay 75 return sleep 1.1 key --delay 75 alt $(for i in {1..5} ; do echo -n "sleep .63 key --delay 75 alt ";done) $(for i in {1..3} ; do echo -n "sleep .3261 key --delay 75 alt ";done) sleep .8 key --delay 75 alt sleep .5 key --delay 75 alt sleep .7 key --delay 75 alt sleep .3 key --delay 75 alt
}

meteor_rush() {
    xdotool key --delay 75 return sleep 1 key --delay 75 alt sleep .8 key --delay 75 alt sleep .4 key --delay 75 alt sleep 1 key --delay 75 alt sleep .5 key --delay 75 alt sleep 1 key --delay 75 alt
}

murder_on_demand() {
    local scrollString=("key --delay 75 return sleep .88 key --delay 75 alt sleep .4 key --delay 75 alt")
    for i in {1..3} ; do
        scrollString+=("sleep 1.05 key --delay 75 alt")
    done
    scrollString+=("sleep .8 key --delay 75 alt")
    scrollString+=("sleep 1 key --delay 75 alt")
    for i in {1..2} ; do
        scrollString+=("sleep .88 key --delay 75 alt")
    done
    scrollString+=("sleep .58 key --delay 75 alt")
    scrollString+=("sleep .89 key --delay 75 alt")
    for i in {6..4..-1} ; do
        scrollString+=("sleep .${i} key --delay 75 alt")
    done
    for i in {1..4} ; do
        scrollString+=("sleep .4 key --delay 75 alt")
    done
    xdotool ${scrollString[*]}
}

oblivion_dance() {
    xdotool key --delay 75 return sleep 1.5 key --delay 75 alt sleep 1.25 key --delay 75 alt sleep .5 key --delay 75 alt
}

r_avenging_spirit() {
    xdotool key --delay 75 return sleep .79 key --delay 75 alt $(for i in {1..7} ; do echo -n "sleep .385 key --delay 75 alt ";done) sleep .6 key --delay 75 alt sleep .7 key --delay 75 alt
}

r_heavens_winds() {
    xdotool key --delay 75 return sleep .66 key --delay 75 alt $(for i in {1..7} ; do echo -n "sleep .32 key --delay 75 alt ";done)
}

unending_curse() {
    xdotool key --delay 75 return sleep 5.53 $(for i in {1..10} ; do echo "key --delay 75 alt sleep .38 ";done)
}

volcanic_storm() {
# xdotool key --delay 250 return key --delay 250 Down key --delay 250 return $(for i in {1..2} ; do echo -n "key --delay 150 Down ";done)
    xdotool key --delay 75 return sleep 10.5 key --delay 75 alt sleep 5.15 key --delay 75 alt sleep .4 key --delay 75 alt sleep .4 key --delay 75 alt
}

sleep .5
case "$1" in
    aura_master) ;&
    avenging_spirit) ;&
    black_hole) ;&
    charge_of_fury) ;&
    death_in_three_easy_steps) ;&
    demons_gift) ;&
    destruction_of_the_heavens) ;&
    diabolical_fire) ;&
    embodiment_of_cruelclaw) ;&
    embodiment_of_power) ;&
    embodiment_of_righteousness) ;&
    embodiment_of_selflessness) ;&
    embodiment_of_valor) ;&
    energy_burst) ;&
    fatal_blast) ;&
    flame_blizzard) ;&
    g_aura_master) ;&
    g_charge_of_fury) ;&
    gods_anger) ;&
    giga_candela) ;&
    heavens_winds) ;&
    infinite_pain) ;&
    infinite_pulse) ;&
    lunar_cannon) ;&
    merciless_vengeance) ;&
    meteor_rush) ;&
    murder_on_demand) ;&
    oblivion_dance) ;&
    r_avenging_spirit) ;&
    r_heavens_winds) ;&
    unending_curse) ;&
    volcanic_storm) $1 ;;
    "-h"|"--help") echo "${usage}";;
    *) exit 1
esac

exit 0
