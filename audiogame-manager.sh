#!/usr/bin/env bash

license() {
    cat << EOF
    ■The contents of this file are subject to the Common Public Attribution
    License Version 1.0 (the ■License■); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at
    https://opensource.org/licenses/CPAL-1.0. The License is based on the Mozilla Public License Version
    1.1 but Sections 14 and 15 have been added to cover use of software over a
    computer network and provide for limited attribution for the Original
    Developer. In addition, Exhibit A has been modified to be consistent with
    Exhibit B.

    Software distributed under the License is distributed on an ■AS IS■ basis,
    WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
    for the specific language governing rights and limitations under the
    License.

    The Original Code is audiogame manager.

    The Original Developer is not the Initial Developer and is . If
    left blank, the Original Developer is the Initial Developer.

    The Initial Developer of the Original Code is Billy "Storm Dragon" Wolfe. All portions of
    the code written by Billy Wolfe are Copyright (c) 2020. All Rights
    Reserved.

    Contributor Michael Taboada.

    Attribution Copyright Notice: Audiogame manager copyright 2020 Storm Dragon. All rights reserved.

    Attribution Phrase (not exceeding 10 words): A Stormux project

    Attribution URL: https://stormgames.wolfe.casa

    Graphic Image as provided in the Covered Code, if any.

    Display of Attribution Information is required in Larger
    Works which are defined in the CPAL as a work which combines Covered Code
    or portions thereof with code not governed by the terms of the CPAL.
EOF
}

# Dialog accessibility
export DIALOGOPTS='--no-lines --visit-items'


# Check for latest news
check_news() {
    trap return INT
    # url for news file
    local newsFile="https://stormgames.wolfe.casa/media/agm.ogg"
    local newsPath="${configFile%/*.conf}/.news"
    local newsTag="$(curl --connect-timeout 5 -sI "$newsFile" | grep -i '^etag: "' | cut -d '"' -f2)"
    if [[ -z "${newsTag}" ]]; then
        return
    fi
    local newsOldTag="$(cat "$newsPath" 2> /dev/null)"
    if [[ "$newsTag" != "$newsOldTag" ]]; then
        dialog --yes-label 'Play' \
            --no-label 'Later' \
            --backtitle 'Audiogame Manager News' \
            --yesno 'Audiogame manager news is available. Please use left and right arrows to navigate and enter to confirm.' -1 -1 || return
        sox -qV0 "$newsFile" -d  &> /dev/null
        echo -n "$newsTag" > "$newsPath"
    fi
}

# Automatic update function
update() {
    local url="$(git ls-remote --get-url)"
    if [[ "$url" =~ ^ssh://|git@ ]] || [[ -z "$url" ]]; then
        return
    fi
    git remote update &> /dev/null
    local upstream='@{u}'
    local home="$(git rev-parse @)"
    local remote="$(git rev-parse "$upstream")"
if [[ "$home" == "$remote" ]]; then
    return
fi
    dialog --backtitle "Audiogame Manager" \
        --yesno "Updates are available. Would you like to update now?" -1 -1 --stdout || return
    git pull
    exit $?
}

# Function to open urls across OS.
open_url() {
    if [[ "$(uname)" == "Darwin" ]]; then
        open "${*}" 2> /dev/null
    else
        xdg-open "${*}" 2> /dev/null
    fi
}

# Create desktop launcher file
desktop_launcher() {
    local desktopFile="${HOME}/audiogame-manager.desktop"
    if [[ -e "${desktopFile}" ]]; then
        echo "the file ${desktopFile} exists. Cannot create the launcher."
        exit 1
    fi
    local dotDesktop
    local terminal
    # Try to find an accessible terminal
    for i in mate-terminal lxterminal terminator gnome-terminal ; do
        if command -v $i &> /dev/null ; then
            terminal="$i"
            break
        fi
    done
    dotDesktop=('[Desktop Entry]'
        'Name=Audiogame manager'
        'GenericName=Audiogame Manager'
        'Comment=Play audio games'
        "Exec=${terminal} -t \"Audiogame Manager\" -e \"/usr/bin/bash -c 'nohup $(readlink -e "$0") 2> /dev/null'\""
        'Terminal=false'
        'Type=Application'
        'StartupNotify=false'
        'Keywords=game;'
        'Categories=Game;'
    'Version=1.0')
    for i in "${dotDesktop[@]}" ; do
        echo "$i" >> "${desktopFile}"
    done
    desktop-file-install --dir "${HOME}/.local/share/applications" -m 755 "${desktopFile}"
    xdg-desktop-icon install ~/.local/share/applications/audiogame-manager.desktop
    rm "${desktopFile}"
    exit 0
}

# Create the clipboard reading function for Sequence Storm
write_sequence_storm_reader() {
if -e ~/.SequenceStormReader ]]; then
        return
    fi
# Here-document start
cat << "EOF" > ~/.SequenceStormReader
#!/usr/bin/env bash

# Wait for the game to be launched
while ! pgrep -u "$USER" ^SequenceStorm &> /dev/null ; do
    sleep 0.05
done

export DISPLAY=:0
unset cliptext
socketFile="$(find /tmp -maxdepth 1 -name "orca-*.sock")"
while pgrep -u "$USER" ^SequenceStorm &> /dev/null ; do
    if [[ "$(uname)" == "Darwin" ]]; then
        tmp="$(pbpaste 2> /dev/null)"
    else
        tmp="$(xclip -selection clipboard -o 2> /dev/null)"
    fi
    tmp="${tmp//%/ percent }"
    if [ "$tmp" != "$cliptext" ] ; then
        cliptext="$tmp"
        if [[ "${cliptext,,}" =~ key|load|private|says|terminal ]]; then
            if [[ "$(uname)" == "Darwin" ]]; then
                say -v alex -r 300 "$cliptext"
            else
                if [[ -w "${socketFile}" ]]; then
                    echo "<#APPEND#>$cliptext" | socat - UNIX-CLIENT:"${socketFile}"
                else
                    spd-say -w -r 50 -- "$cliptext"
                fi
            fi
        else
            if [[ "$(uname)" == "Darwin" ]]; then
                say -v alex -r 300 "$cliptext"
            else
                if [[ -w "${socketFile}" ]]; then
                    echo "$cliptext" | socat - UNIX-CLIENT:"${socketFile}"
                else
                    spd-say -r 50 -- "$cliptext"
                fi
            fi
        fi
    fi
    sleep 0.05
done

exit 0
EOF
# Here-document end
    chmod 755 ~/.SequenceStormReader
}

# Wine configuration section

checklist() {
    declare -a errorList
    declare -a packageList
    if [[ $# -eq 0 ]]; then
        echo "Checking your system..."
        echo
    fi
    if command -v wine &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Wine is installed."
    else
        errorList+=("Critical: Wine is not installed. You will not be able to play any games.")
    fi
    packageList+=("wine")
    if command -v curl &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Curl is installed."
    else
        errorList+=("Critical: Curl is not installed. Critical functionality will not work.")
    fi
    packageList+=("curl")
    if command -v wget &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Wget is installed."
    else
        errorList+=("Critical: Wget is not installed. You will not be able to install any games.")
    fi
    packageList+=("wget")
    if command -v dialog &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Dialog is installed."
    else
        errorList+=("Critical: Dialog is not installed. You will not be able to install, launch, or remove any games.")
    fi
    packageList+=("dialog")
    for i in 7z cabextract unzip xz ; do
        if command -v $i &> /dev/null ; then
            [[ $# -eq 0 ]] && echo "${i^} is installed."
        else
            errorList+=("Critical: ${i^} is not installed. You will not be able to install some games or their components.")
        fi
        packageList+=("$i")
    done
    if command -v gawk &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Gawk is installed."
    else
        errorList+=("Warning: gawk is not installed. Game removal with -r will not work.")
    fi
    packageList+=("gawk")
    if command -v ocrdesktop &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Ocrdesktop is installed."
    else
        errorList+=("Warning: ocrdesktop is not installed. It can help if the installer gets stuck to figure out what is happening.")
    fi
    packageList+=("ocrdesktop")
    if command -v qjoypad &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Qjoypad is installed."
    else
        errorList+=("Warning: qjoypad is not installed. Qjoypad allows you to play keyboard only games with a gamepad.")
    fi
    packageList+=("qjoypad")
    if command -v sox &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Sox is installed."
    else
        errorList+=("Warning: Sox is not installed. Audio will not work.")
    fi
    packageList+=("sox")
    if command -v unix2dos &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Dos2unix is installed."
    else
        errorList+=("Warning: unix2dos is not installed. Some games need a configuration file in dos format before they will run.")
    fi
    packageList+=("unix2dos")
    if command -v w3m &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "W3m is installed."
    else
        errorList+=("Warning: w3m is not installed. W3m is used to view game documentation.")
    fi
    packageList+=("w3m")
    if command -v xclip &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Xclip is installed."
    else
        errorList+=("Warning: Xclip is not installed. Some games may not speak or register properly.")
    fi
    packageList+=("xclip")
    if command -v xdotool &> /dev/null ; then
        [[ $# -eq 0 ]] && echo "Xdotool is installed."
    else
        errorList+=("Warning: Xdotool is not installed. Some installers may not work or may need manual intervention.")
    fi
    packageList+=("xdotool")
    # Show the results
    if [[ $# -ne 0 ]]; then
        for i in "${packageList[@]}" ; do
            echo "$i"
        done | sort
        exit 0
    fi
    if [[ ${#errorList[@]} -eq 0 ]]; then
        echo "No problems found, you are good to go."
        exit 0
    fi
    echo "Errors detected, here is a list along with the severity."
    echo "Note that errors marked critical mean that you will not be able to install and play games until they are resolved."
    for i in "${errorList[@]}" ; do
        echo "$i"
    done
    exit 0
}

clear_cache() {
    local answer
    if [[ ! -d "${cache}" ]]; then
        echo "No cache found at ${cache}."
        return
    fi
    while ! [[ "${answer,,}" =~ ^yes$|^no$ ]]; do
        echo "This will delete all contents of ${cache}. Are you sure you want to continue?"
        echo "Please type yes or no."
        echo
        read -r answer
    done
    if [[ "$answer" == "no" ]]; then
        return
    fi
    # All safety checks done. Delete the cache.
    rm -rfv "${cache}"
    echo "Cache deleted."
}

download() {
    local source=($@)
    for i in "${source[@]}" ; do
        local dest="${i##*/}"
        dest="${dest//%20/ }"
        dest="${dest%\?*}"
        # Remove the destination file if it is empty.
        test -s "${cache}/${dest}" || rm -f "${cache}/${dest}" 2> /dev/null
        if [[ "${redownload}" == "true" ]] && [[ -e "${cache}/${dest}" ]]; then
         rm -v "${cache}/${dest}"
        fi
        # Skip if the item is in cache.
        test -e "${cache}/${dest}" && continue
        if ! wget -4 -O "${cache}/${dest}" "${i}" ; then
            echo "Could not download \"$i\"..."
            exit 1
        fi
    done
}

get_bottle() {
    # Handles games that use the same wine bottle
    case "${game}" in
        # Aprone (Jeremy Kaldobsky) games.
        "castaways"*) ;&
        "castaways-2"*) ;&
        "daytona-and-the-book-of-gold"*) ;&
        "dog-who-hates-toast"*) ;&
        "lunimals"*) ;&
        "paw-prints"*) ;&
        "preludeamals"*) ;&
        "puzzle-divided"*) ;&
        "revelation"*) ;&
        "swamp"*) ;&
        "triple -triad"*) export WINEPREFIX="${HOME}/.local/wine/aprone" ;;
        "bg-"*) export WINEPREFIX="${HOME}/.local/wine/bg";;
        # ESP Pinball games
        "esp-pinball-classic"*) export WINEPREFIX="${HOME}/.local/wine/esp-pinball";;
        "esp-pinball-extreme"*) export WINEPREFIX="${HOME}/.local/wine/esp-pinball";;
        "esp-pinball-party-pack"*) export WINEPREFIX="${HOME}/.local/wine/esp-pinball";;
        # l-works games group
        "duck-hunt"*) ;&
        "judgement-day"*) ;&
        "lockpick"*) ;&
        "pigeon-panic"*) ;&
        "super-egg-hunt"*) ;&
        "super-liam"*) ;&
        "the-great-toy-robbery"*) export WINEPREFIX="${HOME}/.local/wine/l-works";;
        # Nyanchan games group
        "bokurano-daibouken"*) ;&
        "laser-breakout"*) ;&
        "marina-break"*) ;&
        "mp5"*) ;&
        "screaming-strike-2"*) ;&
        "world-of-war"*) export WINEPREFIX="${HOME}/.local/wine/nyanchan";;
        # Oriol Gomez games group
        "bombercats"*) ;&
        "copter-mission"*) ;&
        "danger-on-the-wheel"*) ;&
        "death-on-the-road"*) ;&
        "fuck-that-bird"*) ;&
        "hammer-of-glory"*) ;&
        "insect-therapy"*) ;&
        "rhythm-rage"*) ;&
        "run-for-your-life"*) ;&
        "thief"*) ;&
        "villains-from-beyond"*) export WINEPREFIX="${HOME}/.local/wine/oriol-gomez";;
        # pbgames group
        "dark-destroyer"*) ;&
        "PBGames TMP") export WINEPREFIX="$HOME/.local/wine/pbgames" ;;
        # tunmi13 games group
        "battle-of-the-hunter"*) ;&
        "challenge-of-the-horse"*) export WINEPREFIX="${HOME}/.local/wine/tunmi13";;
        *) export WINEPREFIX="${HOME}/.local/wine/${game%|*}";;
    esac
}

get_installer() {
        trap "exit 0" SIGINT
        # If the file is in cache nothing else needs to be done.
        if [[ -f "${cache}/$1" ]]; then
            return
        fi
        # Create message for dialog.
        local message="Make sure $1 is available in either your Downloads or Desktop directory and press enter to continue."
        if [[ -n "$2" ]]; then
            message+="\n\nThe last good known URL for $game is:"
            message+="\n$2"
        fi
        if echo "$2" | xclip -selection clipboard 2> /dev/null ; then
            message+="\n\nThe URL has been copied to the clipboard."
        fi
        dialog --ok-label "Continue" \
            --backtitle "Audiogame Manager" \
            --msgbox "$message" -1 -1
        # Search the Desktop and Downloads directories for the installation file
        for i in ~/Downloads ~/Desktop ; do
            find $i -type f -name "$1" -exec cp -v {} "${cache}/" \;
        done
        # If the file is still not available abort.
        if [[ ! -f "${cache}/$1" ]]; then
            echo "couldn't find $1. Please download the file and try again."
            exit 1
        fi
}

help() {
    echo "${0##*/}"
    echo "Released under the terms of the Common Public Attribution License Version 1.0"
    echo -e "This is a Stormux project: https://stormux.org\n"
    echo -e "Usage:\n"
    echo "With no arguments, open the game launcher."
    for i in "${!command[@]}" ; do
        echo "-${i/:/ <parameter>}: ${command[${i}]}"
    done | sort
    echo
    echo "Some settings that are often used can be stored in a settings.conf file."
    echo "If wanted, place it at ${configFile%/*}/settings.conf."
    echo "The syntax is variable=\"value\""
    echo
    echo "noCache=\"true\" # Do not keep downloaded items in the cache."
    echo "norh=\"true\" # Do not install RHVoice."
    echo "redownload=\"true\" # Redownload sources, do not use the version stored in cache."
    echo "rhvoice=\"voicename\" # Select the voice to be installed (default Bdl)."
    echo "winedebug=\"flag(s)\" # Set wine debug flags, useful for development."
    exit 0
}

documentation() {
    if [[ "$2" == "Become a Patron" ]]; then
        return
    fi
    if [[ "$2" == "Donate" ]]; then
        return
    fi
    if ! command -v w3m &> /dev/null ; then
        echo "This feature of audiogame-manager requires w3m. Please install it before continuing."
    fi
    get_bottle "$1"
echo "Loading documentation, please wait..."
    # Try to find documentation based on common naming conventions.
    local gamePath="$(winepath -u "$2" 2> /dev/null)"
    gamePath="${gamePath%/*}"
    local gameDoc="$(find "$gamePath" -type f -iname 'user_manual.html' -or -iname 'user_manual.htm' | head -1)"
    # Game name specific docs, add the name to the for loop.
    if [[ -z "$gameDoc" ]]; then
        for i in "troopanum.txt" ; do
            gameDoc="$(find "$gamePath" -type f -iname "$i" -or -iname 'manual.htm' | head -1)"
        done
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -path '*/Manual/index.html' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname '[A-Z]*Help.htm' -or -iname '[A-Z]*Help.html' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname 'manual.html' -or -iname 'manual.htm' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname 'en.html' -or -iname 'en.htm' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname 'readme.html' -or -iname 'readme.htm' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname 'manual.txt' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname 'readme.txt' | head -1)"
    fi
    if [[ -z "$gameDoc" ]]; then
        gameDoc="$(find "$gamePath" -type f -iname '*.url' -exec grep -i 'url=' {} \; | grep -iv 'score' | head -1)"
        gameDoc="${gameDoc#*=}"
        gameDoc="${gameDoc//[[:cntrl:]]/}"
    fi
    # Display documentation if available.
    if [[ -n "$gameDoc" ]]; then
        w3m "$gameDoc"
    else
        echo "No documentation found."
    fi
    exit 0
}

winetricks() {
    # Report used packages to the winetricks maintainer so he knows they are being used.
    if ! [[ -e "${XDG_CACHE_HOME:-$HOME/.cache}/winetricks/track_usage" ]]; then
        mkdir -p "${XDG_CACHE_HOME:-$HOME/.cache}/winetricks/"
        echo "1" > "${XDG_CACHE_HOME:-$HOME/.cache}/winetricks/track_usage"
    fi
    # Download or update agm's copy of winetricks
    if [[ ! -e "${winetricksPath}/winetricks" ]]; then
        checkWinetricksUpdate="true"
        download "https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks"
        mv "${cache}/winetricks" "${winetricksPath}"
        chmod 755 "${winetricksPath}/winetricks"
    else
        if [[ "$checkWinetricksUpdate" != "true" ]]; then
            checkWinetricksUpdate="true"
            ${winetricksPath}/winetricks --self-update
        fi
    fi
    # Run the requested winetricks parameters
    ${winetricksPath}/winetricks "$@"
}

install_rhvoice() {
    if [[ -d "$HOME/.local/wine/${bottle}/drive_c/Program Files/Olga Yakovleva/" ]]; then
        return
    fi
    if [[ "$norh" == "true" ]]; then
        return
    fi
    declare -A RHVoice=(
        [alan]="https://rhvoice.eu-central-1.linodeobjects.com/RHVoice-voice-English-Alan-v4.0.2008.15-setup.exe"
        [bdl]="https://rhvoice.eu-central-1.linodeobjects.com/RHVoice-voice-English-Bdl-v4.1.2008.15-setup.exe"
        [clb]="https://rhvoice.eu-central-1.linodeobjects.com/RHVoice-voice-English-Clb-v4.0.2008.15-setup.exe"
        [slt]="https://rhvoice.eu-central-1.linodeobjects.com/RHVoice-voice-English-Slt-v4.0.2008.15-setup.exe"
    )
    voiceName="${voiceName:-bdl}"
    voiceName="${voiceName,,}"
    if [[ "${RHVoice[${voiceName}]}" == "" ]]; then
        echo "Invalid RHVoice name specified, defaulting to Bdl."
        voiceName="bdl"
    fi
    local voiceFile="${RHVoice[${voiceName}]##*/}"
    download "${RHVoice[${voiceName}]}"
    winetricks -q win8
    echo "Installing RHVoice ${voiceName^}..."
    wine "${cache}/${voiceFile}" &
    sleep 20
    wineserver -k
}

install_wine_bottle() {
    # 32 bit installations work best and are the default here, if you need to override it, do it in the game specific installation steps.
    export WINEARCH="${WINEARCH:-win32}"
    if [[ -z "$bottle" ]]; then
        local bottle="${game,,}"
        bottle="${bottle//[[:space:]]/-}"
        if [[ -d "$HOME/.local/wine/${bottle}" ]]; then
            echo "$HOME/.local/wine/${bottle} exists. Please remove it before running this installer."
            exit 1
        fi
    fi
    export WINEPREFIX="$HOME/.local/wine/${bottle}"
    # Arguments to the function are dependancies to be installed.
        # Get location of mono and gecko.
        monoPath="$(find /usr/share/wine/mono -name "wine-mono*x86.msi" 2> /dev/null)"
        geckoPath="$(find /usr/share/wine/gecko -name "wine-gecko*x86.msi" 2> /dev/null)"
        if [[ -z "$monoPath" ]]; then
            download 'http://dl.winehq.org/wine/wine-mono/6.0.0/wine-mono-6.0.0-x86.msi'
            monoPath="${cache}/wine-mono-6.0.0-x86.msi"
        fi
        if [[ -z "$geckoPath" ]]; then
            download 'http://dl.winehq.org/wine/wine-gecko/2.40/wine_gecko-2.40-x86.msi'
            geckoPath="${cache}/wine_gecko-2.40-x86.msi"
        fi
            # This is in a brace list to pipe through dialog.
            { DISPLAY="" wineboot -u
            wine msiexec /i z:"$monoPath" /quiet
            wine msiexec /i z:"$geckoPath" /quiet
            if [[ "${*}" =~ speechsdk ]]; then
                install_rhvoice
            fi
            if [[ "${WINEARCH}" == "win64" ]]; then
                download "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd"
            fi
            if [[ "${WINEARCH}" == "win64" ]] && [[ ! -f "${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd" ]]; then
                cp "${cache}/nvda2speechd" "${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd"
                chmod +x "${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd"
            fi
            winetricks -q $@ ${winVer:-winxp} ${winetricksSettings}; } | dialog --progressbox "Installing wine bottle, please wait..." -1 -1
}


# Install games
game_installer() {
    mapfile -t installedGames < <(sed '/^$/d' "${configFile}" 2> /dev/null | cut -d '|' -f3)
    # Create the menu of installed games
    declare -a menuList
    for i in "${gameList[@]}" ; do
        local menuItem="$i"
        for j in "${installedGames[@]}" ; do
            if [[ "$j" == "$menuItem" ]]; then
                unset menuItem
            fi
        done
        if [[ -n "$menuItem" ]]; then
            menuList+=("$menuItem" "$menuItem")
        fi
    done
    if [[ ${#menuList[@]} -eq 0 ]]; then
        echo "All games are already installed."
        exit 0
    fi
    menuList+=("Donate" "Donate")
    menuList+=("Become a Patron" "Become a Patron")
    game="$(dialog --backtitle "Audio Game Installer" \
        --clear \
        --no-tags \
        --menu "Please select a game to install" 0 0 0 "${menuList[@]}" --stdout)"
}

# remove games
game_removal() {
    mapfile -t lines < <(sed '/^$/d' "${configFile}" 2> /dev/null)
    if [[ ${#lines} -eq 0 ]]; then
        echo "No games found."
        exit 0
    fi
    # Create the menu of installed games
    declare -a menuList
    for i in "${lines[@]}" ; do
        menuList+=("${i%|*}" "${i##*|}")
    done
    menuList+=("Donate" "Donate")
    menuList+=("Become a Patron" "Become a Patron")
    local game="$(dialog --backtitle "Audio Game Removal" \
        --clear \
        --no-tags \
        --menu "Please select a game to delete" 0 0 0 "${menuList[@]}" --stdout)"
    if [[ ${#game} -gt 0 ]]; then
        if [[ "$game" == "Donate" ]]; then
            open_url "https://ko-fi.com/stormux"
            exit 0
        fi
        if [[ "$game" == "Become a Patron" ]]; then
            open_url "https://2mb.games/product/2mb-patron/"
            exit 0
        fi
        local winePath="${game#*|}"
        export winePath="${winePath%\\*.exe}"
        local wineExec="${game#*|}"
        wineExec="${wineExec%|*}"
        wineExec="${wineExec##*\\}"
        # Confirm removal
        get_bottle "${game%|*}"
        # Make sure the game can be handled by remove
        if [[ "${HOME}/.local/wine/${game%|*}" == "${WINEPREFIX}" ]]; then
            read -rp "To remove the wine bottle \"${WINEPREFIX##*/}\" and all of its contents press enter. To cancel press control+c. " continue
            # kill any previous existing wineservers for this prefix in case they didn't shut down properly.
            wineserver -k
            # remove the game
            rm -rf "${WINEPREFIX}"
        else
            read -rp "This bottle \"${WINEPREFIX##*/}\" contains multiple entries, so only the game will be removed. To continue press enter. To cancel press control+c. " continue
            rm -rf "$(winepath "${winePath}")"
        fi
        # remove the launcher
        gawk -i inplace -vLine="${game//\\/\\\\}" '!index($0,Line)'  "$configFile"
        echo "The selected item has been deleted."
    fi
    exit 0
}

# kill games that are stuck
kill_game() {
    mapfile -t lines < <(sed '/^$/d' "${configFile}" 2> /dev/null)
    if [[ ${#lines} -eq 0 ]]; then
        echo "No games found."
        exit 0
    fi
    # Create the menu of installed games
    declare -a menuList
    for i in "${lines[@]}" ; do
        menuList+=("${i%|*}" "${i##*|}")
    done
    menuList+=("Donate" "Donate")
    menuList+=("Become a Patron" "Become a Patron")
    local game="$(dialog --backtitle "Audio Game Killer" \
        --clear \
        --no-tags \
        --menu "Please select a game to force stop" 0 0 0 "${menuList[@]}" --stdout)"
    if [[ ${#game} -gt 0 ]]; then
        if [[ "$game" == "Donate" ]]; then
            open_url "https://ko-fi.com/stormux"
            exit 0
        fi
        if [[ "$game" == "Become a Patron" ]]; then
            open_url "https://2mb.games/product/2mb-patron/"
            exit 0
        fi
        local winePath="${game#*|}"
        winePath="${winePath%\\*.exe}"
        local wineExec="${game#*|}"
        wineExec="${wineExec%|*}"
        wineExec="${wineExec##*\\}"
        # kill the wine server.
        get_bottle "${game%|*}"
        wineserver -k
        echo "The selected game has been stopped."
    fi
    exit 0
}

# launch games that are installed
game_launcher() {
    mapfile -t lines < <(sed '/^$/d' "${configFile}" 2> /dev/null)
    if [[ ${#lines} -eq 0 ]]; then
        echo "Install some games first."
        exit 0
    fi
    if [[ $# -eq 0 ]]; then
        # Create the menu of installed games
        declare -a menuList
        for i in "${lines[@]}" ; do
            menuList+=("${i%|*}" "${i##*|}")
        done
        menuList+=("Donate" "Donate")
        menuList+=("Become a Patron" "Become a Patron")
        game="$(dialog --backtitle "Audio Game Launcher" \
            --clear \
            --extra-button \
            --extra-label "Documentation" \
            --no-tags \
            --menu "Please select a game to play" 0 0 0 "${menuList[@]}" --stdout)"
        local menuCode=$?
        if [[ $menuCode -eq 1 ]]; then
            exit 0
        elif [[ $menuCode -eq 3 ]]; then
        documentation "$game" "$(echo "$game" | cut -d '|' -f2)"
        fi
    else
        local game="$(grep "^${1}|" "${configFile}" 2> /dev/null)"
        game="${game%|*}"
        if [[ -z "$game" ]]; then
            echo "Game $1 not found."
            exit 1
        fi
    fi
    if [[ ${#game} -gt 0 ]]; then
        if [[ "$game" == "Donate" ]]; then
            open_url "https://ko-fi.com/stormux"
            exit 0
        fi
        if [[ "$game" == "Become a Patron" ]]; then
            open_url "https://2mb.games/product/2mb-patron/"
            exit 0
        fi
        get_bottle "$game"
        local winePath="${game#*|}"
        winePath="${winePath%\\*.exe}"
        local wineExec="${game#*|}"
        wineExec="${wineExec%|*}"
        wineExec="${wineExec##*\\}"
        # kill any previous existing wineservers for this prefix in case they didn't shut down properly.
        wineserver -k
        # launch the game
        # launch the game
        if command -v qjoypad &> /dev/null ; then
            mkdir -p ~/.qjoypad3
            touch "${HOME}/.qjoypad3/${game%|*}.lyt"
            if pgrep qjoypad &> /dev/null ; then
                qjoypad -T "${game%|*}" 2> /dev/null
            else
                qjoypad -T "${game%|*}" 2> /dev/null &
            fi
        fi
        # for games that require custom scripts before launch or custom launch parameters
        if [[ "$game" =~ haunted-party ]]; then
            pgrep -u "$USER" nvda2speechd &> /dev/null || ${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd &
        fi
        if [[ "$game" =~ laser-breakout ]]; then
            pgrep -u "$USER" nvda2speechd &> /dev/null || ${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd &
        fi
        if [[ "$game" =~ ^bokurano-daibouken\||bokurano-daibouken-2\| ]]; then
            "${0%/*}/speech/clipboard_translator.sh" play.exe &
        fi
        if [[ "$game" =~ shadow-line ]]; then
            find "${WINEPREFIX}/drive_c/" -type f -name 'nvdaControllerClient.dll' -exec rm -v "{}" \;
            "${0%/*}/speech/clipboard_translator.sh" play_sr.exe &
        fi
        if [[ "$game" =~ bokurano-daibouken-3 ]]; then
            dictPath="$(winepath "${winePath}")"
            if [[ -r "${cache}/bk3-dict.dat" ]] && [[ ! -d "${dictPath}/dict" ]]; then
                cp "${cache}/bk3-dict.dat" "${dictPath}/dict.dat"
            fi
            if [[ -d "${dictPath}/dict" ]]; then
                if [[ ! -e "${dictPath}/data/nvdaControllerClient.dll" ]]; then
                    cp "${cache}/nvda2speechd32.dll" "${dictPath}/data/nvdaControllerClient.dll"
                fi
                pgrep -u "$USER" nvda2speechd &> /dev/null || ${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd &
            fi
            if [[ ! -d "${dictPath}/dict" ]] && [[ ! -r "${cache}/bk3-dict.dat" ]]; then
                find "${WINEPREFIX}/drive_c/nyanchangame/bk3" -type f -name 'nvdaControllerClient.dll' -exec rm -v "{}" \;
                "${0%/*}/speech/clipboard_translator.sh" play.exe &
            fi
        fi
        if [[ "$game" =~ bop-it-emulator ]]; then
            "${0%/*}/speech/speak_window_title.sh" bop.exe &
        fi
        if [[ "$game" =~ breu2-shadow-hunt ]]; then
            pgrep -u "$USER" nvda2speechd &> /dev/null || ${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd &
        fi
        if [[ "$game" =~ road-to-rage\| ]]; then
            "${0%/*}/speech/speak_window_title.sh" trtr.exe &
        fi
        if [[ "$game" =~ sequence-storm ]]; then
            [[ -x ~/.SequenceStormReader ]] && ~/.SequenceStormReader &
        fi
        if [[ "$game" =~ audiodisc ]]; then
            wine "$winePath\\$wineExec"
            exit 0
        fi
        if [[ "$game" =~ audioquake ]]; then
            wine "$winePath\\$wineExec"
            exit 0
        fi
        if [[ "$game" =~ screaming-strike-2 ]]; then
            pushd "$(winepath "$winePath")"
            wine "$wineExec"
            popd
            exit 0
        fi
        if [[ "$game" =~ warsim ]]; then
            pushd "$(winepath "$winePath")"
            wine "$wineExec"
            popd
            exit 0
        fi
        if [[ "$game" =~ interceptor ]]; then
            pushd "$(winepath "$winePath")"
            wine "$wineExec"
            popd
            exit 0
        fi
        if [[ -d "${WINEPREFIX}/drive_c/windows/syswow64" ]]; then
            pgrep -u "$USER" nvda2speechd &> /dev/null || ${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager/nvda2speechd &
            wine64 start /realtime /d "${winePath}" "$wineExec"
        else
            wine start /d "${winePath}" "$wineExec" /realtime
        fi
    fi
    exit 0
}


# main script

#functions

add_launcher() {
    local launchSettings="${game,,}"
    launchSettings="${launchSettings//[[:space:]]/-}|${1}|${game}"
    if ! grep -F -q -x "${launchSettings}" "${configFile}" 2> /dev/null ; then
        echo "${launchSettings}" >> "${configFile}"
        sort -o "${configFile}" "${configFile}"
        # Remove .lnk files because they don't work.
        find ~/Desktop -type f -iname '*.lnk' -exec bash -c '
            for f ; do
                mimeType="$(file -b "$f")"
                mimeType="${mimeType%%,*}"
                if [[ "$mimeType" == "MS Windows shortcut" ]]; then
                    rm -v "$f"
                fi
            done' _ {} +
        if [[ "${noCache}" == "true" ]]; then
            rm -f "${cache}/${1##*\\}"
        fi
    fi
}

# Check for updates
update
# If display isn't set assume we are launching from console and an X environment is running using display :0
if [[ -z "$DISPLAY" ]]; then
    export DISPLAY=":0"
fi
# Settings file
cache="${XDG_CACHE_HOME:-$HOME/.cache}/audiogame-manager"
configFile="${XDG_CONFIG_HOME:-$HOME/.config}/storm-games/audiogame-manager/games.conf"
mkdir -p "${cache}"
mkdir -p "${configFile%/*}"
# Create the path for AGM's helper programs.
# Originally this was only winetricks, thus the name, and I'm too lazy to change it now.
winetricksPath="${XDG_DATA_HOME:-$HOME/.local/share}/audiogame-manager"
mkdir -p "${winetricksPath}"
# Load any arguments from settings.conf file
if [[ -r "${configFile%/*}/settings.conf" ]]; then
    source "${configFile%/*}/settings.conf"
fi
# Update the cache for older versions of audiogame-manager
if [[ -d "${configFile%/*}/cache" ]]; then
    { mv -v "${configFile%/*}/cache/"* "${cache}"
    rmdir -v "${configFile%/*}/cache/"; } | dialog \
        --backtitle "Audiogame Manager" --progressbox "Updating cache, please wait..." -1 -1
fi
checkWinetricksUpdate="false"
# Turn off debug messages
export WINEDEBUG="${winedebug:--all}"
# Compatibility with box86
export BOX86_NOBANNER=1
# During installation, you can set winVer to the versions available.
# To set winetricks arguments, such as virtual desktop, set the winetricksSettings variable.
# Example: winetricksSettings="vd=1024x768"
# Files are cached unless -N no cache is set.
unset noCache
# Manual installation is not default, make sure it's unset
unset manualInstall


# The list of games available for installation.
# Use menu friendly names.
gameList=(
    "A Hero's Call"
    "Adrian's Doom"
    "Adventurers At C"
    "Alien Outback"
    "AudioDisc"
    "AudioQuake"
    "Battle of the Hunter"
    "Battle Zone"
    #"Beatstar Pro"
    "BG 2048"
    "BG 15 Puzzle"
    "BG Aces Up Solitaire"
    "BG Alchemy"
    "BG Battleship"
    "BG Boggle"
    "BG Boxes"
    "BG Brainiac"
    "BG Chess Challenge"
    "BG Code Breaker"
    "BG Cribbage"
    "BG Cribbage Solitaire"
    "BG Crossword Puzzle"
    "BG Draw Dominoes"
    "BG Fives Dominoes"
    "BG Elevens Solitaire"
    "BG Free Cell Solitaire"
    "BG Golf Solitaire"
    "BG Hangman"
    "BG Hearts"
    "BG Klondike Solitaire"
    "BG LAP"
    "BG Master Mind"
    "BG Mine Sweeper"
    "BG Nomination Whist"
    "BG Penguin Solitaire"
    "BG Poker Solitaire"
    "BG Pyramid Solitaire"
    "BG Scorpion Solitaire"
    "BG Simon"
    "BG Spider Solitaire"
    "BG Scrabble"
    "BG Sudoku"
    "BG Tablic Solitaire"
    "BG Tri-Peaks Solitaire"
    "BG Twenty 20 Cricket"
    "BG Uno"
    "BG Word Builder"
    "BG Word Candy"
    "BG Word Jumble"
    "BG Word Maze"
    "BG Word Solitaire"
    "BG Word Target"
    "BG Word Yahtzee"
    "BG Yahtzee"
    "Blind Drive"
    "Bloodshed"
    "Bokurano Daibouken"
    "Bokurano Daibouken 2"
    "Bokurano Daibouken 3"
    "Bombercats"
    "Bop It Emulator"
    #"Breed Memorial"
    "Breu2 Shadow Hunt"
    "Castaways"
    "Castaways 2"
    "Challenge of the Horse"
    "Chillingham"
    #"Chopper Challenge"
    "Christmas WhoopAss"
    "Code Dungeon"
    "Coin Collector"
    #"Constant Battle"
    "Copter Mission"
    "Crazy Party"
    "Crazy Tennis"
    "Crime Hunter"
    "Danger on the Wheel"
    #"Dark Destroyer"
    "Daytona and the Book of Gold"
    "Death on the Road"
    "Deathmatch"
    "Dog Who Hates Toast"
    #"Dragon Pong"
    "Duck Hunt"
    "DynaMan"
    "Easter Quest"
    #"Endless Runner"
    "Entombed"
    "ESP Pinball Classic"
    "ESP Pinball Extreme"
    "ESP Pinball Party Pack"
    #"Eurofly"
    "Extant"
    #"Fartman"
    "Finger Panic"
    "Fuck That Bird"
    "GMA Tank Commander"
    "Grizzly Gulch"
    "Hammer of Glory"
    "haunted Party"
    "Hearthstone"
    "Hunter"
    "Insect Therapy"
    "Interceptor"
    "Judgement Day"
    "Kitchensinc Games"
    "Kringle Crash"
    #"Laser Breakout"
    #"Light Battles"
    "Light Cars"
    "Lockpick"
    "Lone Wolf"
    "Lunimals"
    "Manamon"
    "Manamon 2"
    #"Marina Break"
    "Minecraft"
    "Monkey Business"
    "MudSplat French"
    "MudSplat English"
    #"MudSplat Swedish"
    "Oh Shit"
    "Operation BlackSquare"
    "Pacman Talks"
    "Palace Punch Up"
    "Paladin of the Sky"
    "Park Boss"
    "Paw Prints"
    "Perilous Hearts"
    "Pontes Kickups!"
    "Pigeon Panic"
    "Preludeamals"
    #"Psycho Strike"
    "Puzzle Divided"
    "Q9"
    "Revelation"
    "Rhythm Rage"
    "River Raiders"
    "Road to Rage"
    "Road to Rage Offline"
    "RS Games"
    "Run For Your Life"
    #"Sammy Center"
    "Scramble!"
    "Screaming Strike 2"
    "Sketchbook"
    "Sequence Storm"
    #"Shades of Doom 1.2"
    "Shades of Doom"
    "Shadow Line"
    "Shooter"
    "Silver Dollar"
    "Slender Lost Vision"
    "Sonic the Hedgehog"
    "Sonic Zoom"
    #"Space Defender"
    "Super Dogs Bone Hunt"
    "Super Egg Hunt"
    "Super Liam"
    "Super Mario Bros"
    "Survive the Wild"
    "Swamp"
    "Technoshock"
    "Ten Pin Alley"
    "The Blind Swordsman"
    #"The Gate"
    "The Great Toy Robbery"
    "The Vale"
    "Thief"
    "Traders of Known Space"
    #"Three D velocity"
    "Tomb Hunter"
    "Top Speed 2"
    "Top Speed 3"
    "Triple Triad"
    "Troopanum2"
    "Tube Sim"
    "Undead Assault"
    "Villains From Beyond"
    "VIP Mud"
    "Warsim"
    #"Wolf Games Launcher"
    "World of War"
)

# Make sure the minimum of curl, sox, wget, wine, and winetricks are installed
for i in curl sox wget wine winetricks ; do
    if ! command -v $i &> /dev/null ; then
        echo "Please install $i before continuing."
        exit 1
    fi
done
# Get latest news if available
check_news
# With no arguments, open the game launcher.
if [[ $# -eq 0 ]]; then
    game_launcher
fi

# Array of command line arguments
declare -A command=(
    [c]="Check your system for necessary components."
    [C]="Clear the cache. All game installers will be deleted."
    [D]="Create desktop shortcut. You can launch audiogame-manager from the desktop or applications menu."
    [d]="Debug mode, wine will be much more verbose when games are installed with this flag."
    [h]="This help screen."
    [i]="Install games."
    [I:]="Noninteractive game installation."
    [k]="Kill a running game that is stuck."
    [L]="Display license information."
    [l:]="Launch given game without interactive audiogame-manager menu specified by its wine bottle."
    [N]="No cache, delete the installer after it has been extracted."
    [n]="No RHVoice, do not install RHVoice when the game is installed."
    [P]="Print a list of packages required by audiogame-manager."
    [R]="Redownload. Removes old versions of packages from cache before installing."
    [r]="Remove a game. This will delete all game data."
    [t]="Total games. Show how many games are currently available."
    [v:]="Select the voice to be installed, default is Bdl."
)

# Convert the keys of the associative array to a format usable by getopts
args="${!command[*]}"
args="${args//[[:space:]]/}"
while getopts "${args}" i ; do
    case "$i" in
        c) checklist;;
        C) clear_cache;;
        D) desktop_launcher;;
        d)
            unset WINEDEBUG
            game_installer
        ;;
        h) help;;
        i) game_installer;;
        I)
            game="${OPTARG}"
            break;;
        k) kill_game;;
        L) license;;
        l) game_launcher "${OPTARG}";;
        N) noCache="true";;
        n) norh="true";;
        P) checklist quiet;;
        R) redownload="true";;
        r) game_removal;;
        t)
            dialog --backtitle "Audiogame Manager" \
                --infobox "There are currently ${#gameList[@]} games available." -1 -1
            exit 0
            ;;
        v) voiceName="${OPTARG}";;
    esac
done

# Install game based on the selection above.
case "${game}" in
    "A Hero's Call")
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle speechsdk corefonts
        # Dotnet is evil. That is all.
        LC_ALL=C DISPLAY="" winetricks -q dotnet462 xna40
        wineserver -k # Really!
        download "http://files.OutOfSightGames.com/files/a-heros-call.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/a-heros-call.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\a-heros-call\A Hero's Call.exe"
    ;;
    "Adrian's Doom")
        install_wine_bottle speechsdk
        download "https://agarchive.net/games/mt/adrian's%20doom.exe"
        wine "${cache}/adrian's doom.exe" /silent
        add_launcher "c:\Program Files\Two Caring Citizens\Adrian's Doom!\adrian.exe"
    ;;
    "Adventurers At C")
        install_wine_bottle speechsdk
        download "http://www.vgstorm.com/aac/aac.zip" "https://www.agarchive.net/games/vg/adventure%20at%20c%20stages.7z"
        unzip -d "$WINEPREFIX/drive_c/Program Files/aac" "${cache}/aac.zip"
        7z e -o"$WINEPREFIX/drive_c/Program Files/aac/stages" "${cache}/adventure at c stages.7z"
        add_launcher "c:\Program Files\aac\aac.exe"
    ;;
    "Alien Outback")
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk quartz
        download "http://download.dracoent.com/Windows/classic/AOSetup.exe"
        wine "${cache}/AOSetup.exe" /sp- /silent
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet20 by itself so it actually doesn't hang.
        winetricks -q dotnet20
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\Alien Outback\ao.exe"
    ;;
    "AudioDisc")
        install_wine_bottle
        download "https://agarchive.net/games/other/audiodisc.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/audiodisc.zip"
        add_launcher "c:\Program Files\audiodisc\disco.exe"
    ;;
    "AudioQuake")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://github.com/matatk/agrip/releases/download/2020.0-beta1/AudioQuake+LDL_2020.0-beta1_Windows.zip" "https://stormgames.wolfe.casa/downloads/quake.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/AudioQuake+LDL_2020.0-beta1_Windows.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/AudioQuake/id1" "${cache}/quake.zip"
        add_launcher "c:\Program Files\AudioQuake\AudioQuake.exe"
        echo
        echo "After you launch the game, press tab then enter and it should begin speaking."
    ;;
    "Battle of the Hunter")
        export bottle="tunmi13"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://tunmi13.ddns.net/projects/bth.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/$game" "${cache}/bth.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\\${game}\bth.exe"
    ;;
    "Battle Zone")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://www.agarchive.net/games/gameMadnessInteractive/battle%20zone%2013.5%20setup.exe"
        wine "${cache}/battle zone 13.5 setup.exe" /silent
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\Battle Zone\ss.exe"
    ;;
    "Beatstar Pro")
        # Sapi is broken on win64 for now, and this game doesn't support nvda it seems.
        export WINEARCH=win64
        export winVer="win7"
        install_wine_bottle mf
        download "https://oriolgomez.com/games/beat_windows.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/Beatstar Pro" "${cache}/beat_windows.zip"
        add_launcher "c:\Program Files\Beatstar Pro\beatstar.exe"
    ;;
    "BG 2048")
        bgInstaller="BG204832Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\BG2048B\BG2048.exe"
    ;;
    "BG 15 Puzzle")
        bgInstaller="FPB32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\FifteenB\FifteenB.exe"
    ;;
    "BG Aces Up Solitaire")
        bgInstaller="ASB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\AcesUpB\AcesUpB.exe"
    ;;
    "BG Alchemy")
        bgInstaller="BAC32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\AlchemyB\AlchemyB.exe"
    ;;
    "BG Battleship")
        bgInstaller="BGB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\BattleshipB\BGBattleship.exe"
    ;;
    "BG Boggle")
        bgInstaller="BGB32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\BoggleB\BoggleB.exe"
    ;;
    "BG Boxes")
        bgInstaller="BXB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\BoxesB\BoxesB.exe"
    ;;
    "BG Brainiac")
        bgInstaller="BRN32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\BrainiacB\BrainiacB.exe"
    ;;
    "BG Chess Challenge")
        bgInstaller="BGC32Setup10d.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\ChessB\BGChess.exe"
    ;;
    "BG Code Breaker")
        bgInstaller="BCB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\CodeBreakerB\BGCodeBreaker.exe"
    ;;
    "BG Cribbage")
        bgInstaller="BGC32Setup12e.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\CribbageB\CribbageB.exe"
    ;;
    "BG Cribbage Solitaire")
        bgInstaller="BCS32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\CribSolB\CribSolB.exe"
    ;;
    "BG Crossword Puzzle")
        bgInstaller="BGX32Setup10h.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\CrosswordB\CrosswordB.exe"
    ;;
    "BG Draw Dominoes")
        bgInstaller="BDD32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\DrawDominoesB\DrawDominoesB.exe"
    ;;
    "BG Elevens Solitaire")
        bgInstaller="ESB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\ElevensB\ElevensB.exe"
    ;;
    "BG Fives Dominoes")
        bgInstaller="BFD32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\FivesDominoesB\FivesDominoesB.exe"
    ;;
    "BG Free Cell Solitaire")
        bgInstaller="BGF32Setup20.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\FreecellB\FreecellB.exe"
    ;;
    "BG Golf Solitaire")
        bgInstaller="GSB32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\GolfSolitaireB\GolfSolitaireB.exe"
    ;;
    "BG Hangman")
        bgInstaller="HMB32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\HangmanB\HangmanB.exe"
    ;;
    "BG Hearts")
        bgInstaller="BGH32Setup10b.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\HeartsB\HeartsB.exe"
    ;;
    "BG Klondike Solitaire")
        bgInstaller="BGK32Setup10b.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\KlondikeB\KlondikeB.exe"
    ;;
    "BG LAP")
        bgInstaller="LAP32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\LAP\LAP.exe"
    ;;
    "BG Master Mind")
        bgInstaller="BMM32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\MastermindB\BGMasterMind.exe"
    ;;
    "BG Mine Sweeper")
        bgInstaller="MSB32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\MinesweeperB\MinesweeperB.exe"
    ;;
    "BG Nomination Whist")
        bgInstaller="BNW32Setup10a.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\NomWhistB\NomWhistB.exe"
    ;;
    "BG Penguin Solitaire")
        bgInstaller="BPS32Setup10c.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\PenguinB\PenguinB.exe"
    ;;
    "BG Poker Solitaire")
        bgInstaller="BPS32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\PokerSolB\PokerSolB.exe"
    ;;
    "BG Pyramid Solitaire")
        bgInstaller="PSB32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\PyramidB\PyramidB.exe"
    ;;
    "BG Scorpion Solitaire")
        bgInstaller="BSS32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\ScorpionB\ScorpionB.exe"
    ;;
    "BG Simon")
        bgInstaller="BGS32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\SimonB\SimonB.exe"
    ;;
    "BG Spider Solitaire")
        bgInstaller="SPB32Setup10b.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\SpiderB\SpiderB.exe"
    ;;
    "BG Scrabble")
        bgInstaller="BGS32Setup20.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\ScrabbleB\ScrabbleB.exe"
    ;;
    "BG Sudoku")
        bgInstaller="SDB32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\SudokuB\SudokuB.exe"
    ;;
    "BG Tablic Solitaire")
        bgInstaller="TSB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\TabSolB\BGTabSol.exe"
    ;;
    "BG Tri-Peaks Solitaire")
        bgInstaller="TPB32Setup10a.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\TriPeaksB\TriPeaksB.exe"
    ;;
    "BG Twenty 20 Cricket")
        bgInstaller="T20B32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\T20CricketB\CricketB.exe"
    ;;
    "BG Uno")
        bgInstaller="BGU32Setup11a.exe" "http://www.spoonbillsoftware.com.au/bggames.htm"
        export bottle="bg"
        get_installer "${bgInstaller}"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\UnoB\UnoB.exe"
    ;;
    "BG Word Builder")
        bgInstaller="BWB32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordBuilderB\WordBuilderB.exe"
    ;;
    "BG Word Candy")
        bgInstaller="WCB32Setup10a.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordCandyB\WordCandyB.exe"
    ;;
    "BG Word Jumble")
        bgInstaller="BWJ32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordJumbleB\WordJumbleB.exe"
    ;;
    "BG Word Maze")
        bgInstaller="BWM32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordMazeB\WordMazeB.exe"
    ;;
    "BG Word Solitaire")
        bgInstaller="WSB32Setup10.exe"
        export bottle="bg"
        get_installer "$bgInstaller" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordSolitaireB\WordSolitaireB.exe"
    ;;
    "BG Word Target")
        bgInstaller="WTB32Setup10a.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordTargetB\WordTargetB.exe"
    ;;
    "BG Word Yahtzee")
        bgInstaller="BWY32Setup10.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\WordYahtzeeB\BGWordYahtzee.exe"
    ;;
    "BG Yahtzee")
        bgInstaller="BGY32Setup10a.exe"
        export bottle="bg"
        get_installer "${bgInstaller}" "http://www.spoonbillsoftware.com.au/bggames.htm"
        install_wine_bottle speechsdk
        wine "${cache}/${bgInstaller}" /silent
        add_launcher "c:\Program Files\Games\yahtzeeB\BGYahtzee.exe"
    ;;
    "Blind Drive")
        get_installer "Blind Drive 1.1.112.00i-win64.zip" "https://lofipeople.itch.io/blind-drive"
        download "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        export WINEARCH=win64
        export winVer="win8"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/blind-drive" "${cache}/Blind Drive 1.1.112.00i-win64.zip"
        # Weird work around to get keyboard working.
        winetricks -q usetakefocus=y
        winetricks -q usetakefocus=n
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\blind-drive\Blind Drive.exe"
    ;;
    "Bloodshed")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.samtupy.com/games/bloodshed.exe"
        cp "${cache}/bloodshed.exe" "$WINEPREFIX/drive_c/Program Files/"
        add_launcher "c:\Program Files\bloodshed.exe"
    ;;
    "Bokurano Daibouken")
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle
        download "https://www.nyanchangames.com/softs/nn_setup.exe"
        7z x -o"$WINEPREFIX/drive_c/nyanchangame/bk" "${cache}/nn_setup.exe"
        find "${WINEPREFIX}/drive_c/nyanchangame/bk/" -type f -name 'nvdaControllerClient.dll' -exec rm -v "{}" \;
        add_launcher "c:\nyanchangame\bk\play.exe"
    ;;
    "Bokurano Daibouken 2")
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle
        download "https://www.nyanchangames.com/softs/nn2_setup.exe"
        7z x -o"$WINEPREFIX/drive_c/nyanchangame/bk2" "${cache}/nn2_setup.exe"
        find "${WINEPREFIX}/drive_c/nyanchangame/bk2/" -type f -name 'nvdaControllerClient.dll' -exec rm -v "{}" \;
        add_launcher "c:\nyanchangame\bk2\play.exe"
    ;;
    "Bokurano Daibouken 3")
        if [[ ! -r "${cache}/bk3-dict.dat" ]]; then
            echo "http://www.nyanchangames.com/order/bk3translate.html" | xclip -selection clipboard 2> /dev/null
            dialog --backtitle "Audiogame manager" --msgbox "If you would like English translations, the file is available at http://www.nyanchangames.com/order/bk3translate.html. Save the dict.dat file to your Downloads or Desktop directory. For convenience the url has been copied to your clipboard. Press enter when you are ready to continue." -1 -1 --stdout 
        fi
        dictFile=""
        for i in "${HOME}/Downloads/dict.dat" "${HOME}/Desktop/dict.dat" ; do
            if [[ -r "$i" ]]; then
                dictFile="$i"
            fi
        done
        if [[ "${#dictFile}" -ge 3 ]] && [[ ! -r "${cache}/bk3-dict.dat" ]]; then
            dialog --backtitle "Audiogame manager" --yesno "Possible English translation file found at $dictFile. Would you like to use it for BK3?" -1 -1 --stdout && cp -v "$dictFile" "${cache}/bk3-dict.dat"
        fi
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle
        download "https://www.nyanchangames.com/softs/nn3_setup.exe" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd32.dll"
        7z x -o"$WINEPREFIX/drive_c/nyanchangame/bk3" "${cache}/nn3_setup.exe"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient.dll' -exec cp -v "${cache}/nvda2speechd32.dll" "{}" \;
        add_launcher "c:\nyanchangame\bk3\play.exe"
    ;;
    "Bop It Emulator")
        export winVer="win7"
        install_wine_bottle
        download "http://www.masonasons.me/softs/BopItEmulator3.1PasswordIsBopIt.7z"
        7z x -o"$WINEPREFIX/drive_c/Program Files/Bop It" "${cache}/BopItEmulator3.1PasswordIsBopIt.7z" -pBopIt
        add_launcher "c:\Program Files\Bop It\bop.exe"
    ;;
    "Breu2 Shadow Hunt")
        get_installer "breu2.zip" "https://breu.itch.io/shadowhunt"
        download "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        export WINEARCH=win64
        export winVer="win8"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/breu2.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\breu2\breu2.exe"
    ;;
    "Bombercats")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/bombercats_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/bomvercats" "${cache}/bombercats_en.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\bomvercats\game.exe"
    ;;
    "Breed Memorial")
        export winVer="win7"
        install_wine_bottle cjkfonts speechsdk
        download "https://hirotaka2014.sakura.ne.jp/mh0406/game/breed_memorial.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/breed_memorial.zip"
        #find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\Breed memorial\Breed memorial\breed memorial.exe"
    ;;
    "Castaways")
        export bottle="aprone"
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk
        download "https://www.kaldobsky.com/audiogames/castaways.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/castaways" "${cache}/castaways.zip"
        wine "c:\Program Files\castaways\Checkup.exe" /verysilent
        add_launcher "c:\Program Files\castaways\Castaways.exe"
    ;;
    "Castaways 2")
        export bottle="aprone"
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://www.kaldobsky.com/audiogames/castaways2beta.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/castaways2" "${cache}/castaways2beta.zip"
        wine "c:\Program Files\castaways2\Checkup.exe" /verysilent
        add_launcher "c:\Program Files\castaways2\Castaways2.exe"
    ;;
    "Challenge of the Horse")
        export bottle="tunmi13"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://tunmi13.ddns.net/projects/coth.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/$game" "${cache}/coth.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\\${game}\game.exe"
    ;;
    "Chillingham")
        install_wine_bottle vb6run mfc42
        download "https://stormgames.wolfe.casa/downloads/chillingham.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "$cache/chillingham.zip"
        add_launcher "c:\Program Files\chillingham\Chillingham.exe"
    ;;
    "Chopper Challenge")
        # Freezes at menu
        install_wine_bottle vb6run dx8vb speechsdk
        download "https://www.agarchive.net/games/XSight/chopper%20challenge%20setup.exe"
        wine "${cache}/chopper challenge setup.exe" /silent &
        xdotool sleep 5 key y 2> /dev/null
        wineserver -w
        echo "$USER|n/a" >> "$WINEPREFIX/drive_c/Program Files/x-sight interactive/chopper challenge/config.dat"
        add_launcher "c:\Program Files\x-sight interactive\chopper challenge\Chopper.exe"
    ;;
    "Coin Collector")
        export WINEARCH=win64
        export winVer="win7"
        download "https://www.dropbox.com/s/v55q7t9n84otmcd/coin%20collector.rar?dl=1" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        install_wine_bottle
        unrar x "$cache/coin collector.rar" -op"$WINEPREFIX/drive_c/Program Files"
        find "$WINEPREFIX/drive_c/Program Files/coin collector" -type f -name "nvdaControllerClient64.dll" -exec cp -v "$cache/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\coin collector\game.exe"
    ;;
    "Constant Battle")
        export winVer="win7"
        install_wine_bottle cjkfonts speechsdk
        download "https://renovagames.com/bc/BC-Setup.exe"
        wine "${cache}/BC-Setup.exe" /silent
        #add_launcher "c:\Program Files\"
    ;;
    "Christmas WhoopAss")
        install_wine_bottle vb6run dx8vb
        download "https://www.agarchive.net/games/draconis/christmas%20whoopass%20setup.exe"
        wine "${cache}/christmas whoopass setup.exe" /sp- /silent
        add_launcher "c:\Program Files\Draconis Entertainment\Christmas Whoop Ass\wa.exe"
    ;;
    "Code Dungeon")
        get_installer "codedungeon-win-64.zip" "https://stealcase.itch.io/codedungeon"
        download "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        export WINEARCH=win64
        export winVer="win8"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/code-dungeon" "${cache}/codedungeon-win-64.zip"
        # Weird work around to get keyboard working.
        winetricks -q usetakefocus=y
        winetricks -q usetakefocus=n
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\code-dungeon\Code Dungeon.exe"
    ;;
    "Copter Mission")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/copter_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/copter mission" "${cache}/copter_en.zip"
        add_launcher "c:\Program Files\copter mission\game.exe"
    ;;
    "Crazy Party")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://pragmapragma.free.fr/crazy-party/Crazy-Party-beta78.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/Crazy-Party-beta78.zip"
        add_launcher "c:\Program Files\Crazy-Party-beta78\Crazy Party.exe"
        rm -f "${WINEPREFIX}/drive_c/Program Files/Crazy-Party-beta78/nvdaControllerClient32.dll"
    ;;
    "Crazy Tennis")
        install_wine_bottle speechsdk
        download "https://www.agarchive.net/games/VIP/crazy%20tennis%20setup.exe"
        wine "${cache}/crazy tennis setup.exe" /sp- /silent
        add_launcher "c:\Program Files\Crazytennis\crazytennis.exe"
    ;;
    "Crime Hunter")
        export WINEARCH=win64
        export winVer="win7"
        install_wine_bottle
        download "http://masonasons.me/softs/CH2.0Win.zip" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/crime-hunter" "${cache}/CH2.0Win.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient64.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\crime-hunter\ch.exe"
    ;;
    "Danger on the Wheel")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://oriolgomez.com/games/wheel_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/danger on the wheel" "${cache}/wheel_en.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\danger on the wheel\game.exe"
    ;;
    "Dark Destroyer")
        export bottle=pbgames
        install_wine_bottle speechsdk ie6
        download "https://www.agarchive.net/games/pb/Dark-Destroyer-Setup.exe"
        wine "$cache/Dark-Destroyer-Setup.exe" /silent
        add_launcher 'c:\Pbgames\Dark_destroyer\darkdestroyer.exe'
    ;;
    "Daytona and the Book of Gold")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz corefonts
        download "https://kaldobsky.com/audiogames/Daytona.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/daytona" "${cache}/Daytona.zip"
        wine 'c:\Program Files\daytona\checkup.exe' /verysilent
        add_launcher "c:\Program Files\daytona\Daytona.exe"
;;
"Death on the Road")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/road_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/death on the road" "${cache}/road_en.zip"
        add_launcher "c:\Program Files\death on the road\game.exe"
    ;;
    "Deathmatch")
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle quartz speechsdk
        download "https://www.agarchive.net/games/realitySoftware/death%20match%20project%20alpha%20setup.exe"
        wine "${cache}/death match project alpha setup.exe" /silent
        add_launcher "c:\Program Files\reality software\death match project alpha\dm1.exe"
    ;;
    "Dog Who Hates Toast")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz speechsdk corefonts
        download "https://www.kaldobsky.com/audiogames/dogwhohatestoast.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/dogwhohatestoast" "${cache}/dogwhohatestoast.zip"
        wine 'c:\Program Files\dogwhohatestoast\checkup.exe' /verysilent
        add_launcher "c:\Program Files\dogwhohatestoast\DogwhoHatesToast.exe"
        echo "Note: Dog who Hates Toast installed. Once you start the game, you must press tab until you hear sapi on to get speech." >&2
;;
    "Dragon Pong")
        install_wine_bottle vb6run dx8vb speechsdk
        download "https://www.iamtalon.me/games/dragonpong.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/dragonpong.zip"
        add_launcher "c:\Program Files\dragonpong\DragonPong.exe"
    ;;
    "Duck Hunt")
        export bottle="l-works"
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://files.l-works.net/dhsetup.exe"
        wine "${cache}/dhsetup.exe" /silent
        add_launcher "c:\Program Files\Lworks\Duck Hunt\duckhunt.exe"
    ;;
    "DynaMan")
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk quartz
        download "http://download.dracoent.com/Windows/classic/DMSetup.exe"
        wine "${cache}/DMSetup.exe" /sp- /silent
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet20 by itself so it actually doesn't hang.
        winetricks -q dotnet20
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\DynaMan\dm.exe"
    ;;
    "Easter Quest")
        install_wine_bottle
        download "https://agarchive.net/games/mt/easter%20quest%20setup.exe"
        wine "${cache}/easter quest setup.exe" /silent
        add_launcher "c:\Program Files\MTGames\Easter Quest\easter.exe"
    ;;
    "Endless Runner")
        install_wine_bottle speechsdk
        download "http://www.masonasons.me/softs/EndlessRunner.7z"
        7z e -y -o"$WINEPREFIX/drive_c/Program Files/Endless Runner" "${cache}/EndlessRunner.7z" -prunner
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\Endless Runner\runner.exe"
    ;;
    "Entombed")
        export winVer="win7"
        install_wine_bottle speechsdk msvcrt40 gdiplus ie7 wmp11 mf
        # Ok, more dotnet.
        LC_ALL=C DISPLAY="" winetricks -q dotnet40 xna40
        wineserver -k # Sigh.
        download "http://blind-games.com/newentombed/EntombedSetup.exe" "https://download.microsoft.com/download/E/C/1/EC1B2340-67A0-4B87-85F0-74D987A27160/SSCERuntime-ENU.exe" "https://stormgames.wolfe.casa/downloads/Entombed.exe.config" "https://stormgames.wolfe.casa/downloads/mfplat.dll"
        mkdir -p "${WINEPREFIX}/drive_c/temp"
        pushd "${WINEPREFIX}/drive_c/temp"
        7z e "${cache}/SSCERuntime-ENU.exe"
        wine msiexec /i "${WINEPREFIX}/drive_c/temp/SSCERuntime_x86-ENU.msi" /q
        rm *
        popd
        pushd "${WINEPREFIX}/drive_c/Program Files/Microsoft SQL Server Compact Edition/v3.5"
        wine regsvr32 sqlceoledb35.dll
        wine regsvr32 sqlceca35.dll
        popd
        wine "${cache}/EntombedSetup.exe" /silent
        pushd "${WINEPREFIX}/drive_c/Program Files/Entombed"
        cp ../Microsoft\ SQL\ Server\ Compact\ Edition/v3.5/Private/System.Data.SqlServerCe.Entity.dll ../Microsoft\ SQL\ Server\ Compact\ Edition/v3.5/Private/System.Data.SqlServerCe.dll .
        cp ../Microsoft\ SQL\ Server\ Compact\ Edition/v3.5/sql* .
        cp "${cache}/Entombed.exe.config" .
        popd
        if [ ! -f "${WINEPREFIX}/drive_c/windows/system32/mfplat.dll" ] ; then
            cp "${cache}/mfplat.dll" "${WINEPREFIX}/drive_c/windows/system32/"
        fi
        add_launcher "c:\Program Files\Entombed\Entombed.exe"
    ;;
    "ESP Pinball Classic")
        export winVer="win7"
        export bottle="esp-pinball"
        install_wine_bottle vb6run dx8vb speechsdk quartz
        download "http://download.dracoent.com/Windows/classic/PBCSetup.exe"
        wine "${cache}/PBCSetup.exe" /sp- /silent
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet20 by itself so it actually doesn't hang.
        winetricks -q dotnet20
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\ESP Pinball Classic\pbc.exe"
    ;;
    "ESP Pinball Extreme")
        export winVer="win7"
        export bottle="esp-pinball"
        install_wine_bottle vb6run dx8vb speechsdk quartz
        download "http://download.dracoent.com/Windows/classic/PBXSetup.exe"
        wine "${cache}/PBXSetup.exe" /sp- /silent
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet20 by itself so it actually doesn't hang.
        winetricks -q dotnet20
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\ESP Pinball Xtreme\pbx.exe"
    ;;
    "ESP Pinball Party Pack")
        export winVer="win7"
        export bottle="esp-pinball"
        export WINEPREFIX="$HOME/.local/wine/esp-pinball"
        # Only works in conjunction with esp pinball extreme.
        if ! [ -f "$HOME/.local/wine/$bottle/drive_c/Program Files/Draconis Entertainment/ESP Pinball Xtreme/pbx.exe" ] ; then
            echo "Error: You need to install ESP Pinball Extreme first to use this game. Please do so before continuing." >&2
            exit 1
        fi
        download "http://download.dracoent.com/Windows/classic/PP1Setup.exe"
        wine "${cache}/PP1Setup.exe" /sp- /silent
        add_launcher "c:\Program Files\Draconis Entertainment\ESP Pinball Xtreme\pbx.exe"
    ;;
    "Eurofly")
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle speechsdk comctl32
        download "https://www.stefankiss.sk/files/eurofly2/Launcher_1.2.zip" "https://www.stefankiss.sk/files/eurofly2/Eurofly_2_ful_setup.exe" 
        wine "${cache}/Eurofly_2_ful_setup.exe" /silent
        unzip -o -d "$WINEPREFIX/drive_c/Eurofly" "${cache}/Launcher_1.2.zip"
        add_launcher "c:\Eurofly\launcher.exe"
        echo "Note: On first and sometimes later launch, Eurofly may take a very long time to download required files, please be patient..."
    ;;
    "Extant")
        install_wine_bottle speechsdk
        download "https://agarchive.net/games/other/extant.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/extant" "${cache}/extant.zip"
        add_launcher "c:\Program Files\extant\Extant.exe"
    ;;
    "Fartman")
        install_wine_bottle dx8vb vb6run
        download "http://www.agarchive.net/games/bpc/fartman.exe"
        wine "${cache}/fartman.exe" /silent
        #add_launcher "c:\Program Files\"
    ;;
    "Finger Panic")
        install_wine_bottle dx8vb vb6run
        download "http://www.agarchive.net/games/bsc/FingerPanicSetup.exe"
        wine "${cache}/FingerPanicSetup.exe" /sp- /silent
        add_launcher "c:\Program Files\Finger Panic 1.0\FingerPanic.exe"
    ;;
    "Fuck That Bird")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/bird_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/fuck that bird" "${cache}/bird_en.zip"
        add_launcher "c:\Program Files\fuck that bird\game.exe"
    ;;
    "GMA Tank Commander")
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://www.gmagames.com/gtc120.exe"
        wine "${cache}/gtc120.exe" /silent &
        xdotool sleep 10 key --clearmodifiers alt+y 2> /dev/null
        xdotool sleep 15 type --clearmodifiers --delay 100 "${USER^}" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool sleep 10 type --clearmodifiers --delay 100 "${HOSTNAME^}" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool sleep 10 type --clearmodifiers --delay 100 "na@na.na" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool sleep 10 type --clearmodifiers --delay 100 "uuuuuuu" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool key --clearmodifiers Return 2> /dev/null
        xdotool sleep 10 key --clearmodifiers Return 2> /dev/null
        wineserver -w
        echo "To accurately set your information, edit the file:"
        echo "${WINEPREFIX}/drive_c/Program\ Files/GMA\ Tank\ Commander/config.dat"
        echo "The default country is US. The fields are:"
        echo -e "\"Firstname Lastname\"\n\"Email address\"\n\"Country code\""
        add_launcher "c:\Program Files\GMA Tank Commander\gtc.exe"
    ;;
    "Grizzly Gulch")
        install_wine_bottle vb6run mfc42
        download "https://stormgames.wolfe.casa/downloads/grizzly-gulch.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "$cache/grizzly-gulch.zip"
        add_launcher "c:\Program Files\grizzly-gulch\Grizzly Gulch.exe"
    ;;
    "Hammer of Glory")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://oriolgomez.com/games/hammer_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/hammer of glory" "${cache}/hammer_en.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\hammer of glory\game.exe"
    ;;
    "haunted Party")
        get_installer "hp.zip" "https://tunmi13.itch.io/haunted-party"
        download "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        export WINEARCH=win64
        export winVer="win8"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/hp.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient64.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\hp\hp.exe"
    ;;
    "Hearthstone")
        export winVer="win10"
        install_wine_bottle ie8 vcrun2015 speechsdk
        # This download url breaks the download function, so grab it manually.
        wget -O "${cache}/Hearthstone.exe" "https://us.battle.net/download/getInstaller?os=win&installer=Hearthstone-Setup.exe"
        wget -O "${cache}/Hearthstone_patch.zip" "https://github.com/HearthstoneAccess/HearthstoneAccess/releases/latest/download/patch.zip"
        wine "${cache}/Hearthstone.exe" /silent
        unzip -o "${cache}/Hearthstone_patch.zip" -d "${WINEPREFIX}/drive_c/Program Files/Hearthstone"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        #add_launcher "c:\Program Files\"
    ;;
    "Hunter")
        install_wine_bottle vb6run dx8vb
        # FIXME: Hacky, but it works. Install dotnet35 by itself so it actually doesn't hang.
        DISPLAY="" winetricks -q dotnet35
        wineserver -k # Damn you, dotnet.
        download "http://www.agarchive.net/games/bsc/HunterSetup.exe" "https://www.agarchive.net/games/bsc/BSC%20unlock%20code%20generator.7z"
        wine "${cache}/HunterSetup.exe" /silent &
        xdotool sleep 10 key --clearmodifiers alt+y 2> /dev/null
        sleep 30 && wineserver -k # Sometimes the installer finishes but the wineserver has more processes that don't exit, so we can't depend on wineserver -w.
        mkdir -p "$WINEPREFIX/drive_c/Program Files/bsc-key-generator"
        7z e -o"$WINEPREFIX/drive_c/Program Files/bsc-key-generator" "${cache}/BSC unlock code generator.7z"
        echo "$USER"$'\n'"$(hostname)"$'\n'"none"$'\n'"US" > "$WINEPREFIX/drive_c/Program Files/Hunter/config.dat"
        command -v unix2dos &> /dev/null && unix2dos "$WINEPREFIX/drive_c/Program Files/Hunter/config.dat" || echo "Warning! You do not have unix2dos or dos2unix installed, Hunter may not work!"
        if command -v xclip &> /dev/null && command -v xdotool &> /dev/null ; then
            wine "c:\Program Files\Hunter\HunterRegistration.exe" &
            xdotool sleep 10 key Return sleep 2 key Return sleep 2 key Alt+n
            sleep 1
            regcode="$(xclip -selection clipboard -o)"
            xdotool sleep 1 key Shift+Tab sleep 1 key Return
            # FIXME: Kind of hacky, but let's make sure it actually exitted.
            sleep 5
            wineserver -k
            wine "c:\Program Files\bsc-key-generator\BlindsoftwareUnlockCodeGenerator.exe" &
            xdotool sleep 10 key Return sleep 2 type h
            xdotool sleep 1 key Tab sleep 1 type $regcode
            xdotool sleep 1 key Tab sleep 1 key Return
            sleep 2
            regcode="$(xclip -selection clipboard -o)" # Might as well reuse the variable.
                        # FIXME: Kind of hacky, but let's make sure it actually exitted since I can't find a good way to exit this program.
            sleep 5
            wineserver -k
                     wine "c:\Program Files\Hunter\HunterRegistration.exe" &
            echo "$regcode" | xclip -selection clipboard
            xdotool sleep 10 key Return sleep 2 key Shift+Tab sleep 1 key Shift+Tab sleep 1 key Return
            # FIXME: Kind of hacky, but let's make sure it actually exitted.
            sleep 5
            wineserver -k
        else
            echo "Warning: You need xclip and xdotool for this installer to finish the registration process, however, you don't have them. Either remove the program, install the stated tools, and reinstall it, or go on your own. Caveat Emptor, go to jail, do not pass go, do not collect audiogame rewards and all that stuff."
        fi
        add_launcher "c:\Program Files\Hunter\HunterRun.exe"
    ;;
    "Insect Therapy")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/insect_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/insect therapy" "${cache}/insect_en.zip"
        add_launcher "c:\Program Files\insect therapy\game.exe"
    ;;
    "Interceptor")
        install_wine_bottle
        download "http://www.valiantgalaxy.com/interceptor/Interceptor0.0.2.0Installer.exe"
        wine "${cache}/Interceptor0.0.2.0Installer.exe" /silent
        add_launcher "c:\Program Files\VGA\interceptor\launch_interceptor.exe"
    ;;
    "Judgement Day")
        export bottle="l-works"
        install_wine_bottle vb6run dx8vb quartz
        download "http://files.l-works.net/judgmentdayfullsetup.exe"
        wine "${cache}/judgmentdayfullsetup.exe" /silent
cat << EOF > /tmp/judgementday.reg
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\VB and VBA Program Settings\judgmentday\config]
"name"="$USER"
EOF
        wine regedit /s /tmp/judgementday.reg
        rm /tmp/judgementday.reg
        add_launcher "c:\Program Files\Lworks\Judgment Day\judgmentday.exe"
    ;;
    "Kitchensinc Games")
        install_wine_bottle vb6run speechsdk dx8vb
        download "https://stormgames.wolfe.casa/downloads/kitchen.tar.xz"
        echo "Extracting files..."
        tar xf "${cache}/kitchen.tar.xz" -C "$WINEPREFIX/drive_c/Program Files/"
        add_launcher "c:\Program Files\Kitchen's Sink\gamemenu.exe"
    ;;
    "Kringle Crash")
        install_wine_bottle
        download "https://www.agarchive.net/games/blastbay/kringle%20crash%20setup.exe"
        wine "${cache}/kringle crash setup.exe" /silent
        add_launcher "c:\Program Files\Kringle Crash\kringlecrash.exe"
    ;;
    "Laser Breakout")
        # Currently only speaks in japanese, looking to see if we can find an english version.
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle
        download "https://www.agarchive.net/games/nyanchan/laser%20breakout.7z" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd32.dll"
        7z e -o"$WINEPREFIX/drive_c/nyanchangame/laser breakout" "$cache/laser breakout.7z"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient.dll' -exec cp -v "${cache}/nvda2speechd32.dll" "{}" \;
        add_launcher "c:\nyanchangame\laser breakout\play.exe"
    ;;
    "Light Battles")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://prometheus-enterprises.com/games/CoL.exe"
        7z e -o"$WINEPREFIX/drive_c/Program Files/Light Battles" "${cache}/CoL.exe"
        rm -fv "${WINEPREFIX}/drive_c/Program Files/Light Battles/nvdaControllerClient.dll"
        add_launcher "c:\Program Files\Light Battles\battles.exe"
    ;;
    "Light Cars")
        install_wine_bottle dx8vb vb6run
        download "https://www.agarchive.net/games/lighttech/light%20cars%20setup.exe"
        wine "${cache}/light cars setup.exe" &
        xdotool sleep 10 key --clearmodifiers alt+n sleep 1 key --clearmodifiers alt+a key --clearmodifiers space sleep 1 key --clearmodifiers alt+n sleep 1 key --clearmodifiers alt+n sleep 1 key --clearmodifiers alt+i sleep 30 key --clearmodifiers Tab sleep 1 key --clearmodifiers Return 2> /dev/null
        wineserver -w
        echo -e "${USER} ${HOST}\nna@na.na\nUS" > ~/.local/wine/light-cars/drive_c/Program\ Files/Lighttech\ Interactive/Light\ Cars/config.dat
        unix2dos ~/.local/wine/light-cars/drive_c/Program\ Files/Lighttech\ Interactive/Light\ Cars/config.dat
        add_launcher "c:\Program Files\Lighttech Interactive\Light Cars\lightCars.exe"
    ;;
    "Lockpick")
        export bottle="l-works"
        install_wine_bottle vb6run dx8vb
        download "http://files.l-works.net/lockpicksetup.exe"
        wine "${cache}/lockpicksetup.exe" /silent
        add_launcher "c:\Program Files\lWorks\Lockpick\lockpick.exe"
    ;;
    "Lone Wolf")
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://www.gmagames.com/lw350.exe"
        wine "${cache}/lw350.exe" /silent &
        xdotool sleep 10 key --clearmodifiers alt+y 2> /dev/null
        xdotool sleep 10 type --clearmodifiers --delay 100 "$USER" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool type --clearmodifiers --delay 100 "$HOSTNAME" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool type --clearmodifiers --delay 100 "na@na.na" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool type --clearmodifiers --delay 100 "uuuuuu" 2> /dev/null
        xdotool key --clearmodifiers Tab 2> /dev/null
        xdotool key --clearmodifiers Return 2> /dev/null
        wineserver -w
        add_launcher "c:\Program Files\Lone Wolf\lw.exe"
    ;;
    "Lunimals")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz speechsdk corefonts
        download "https://kaldobsky.com/audiogames/lunimals.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/lunimals" "${cache}/lunimals.zip"
        wine 'c:\Program Files\lunimals\checkup.exe' /verysilent
        add_launcher "c:\Program Files\lunimals\Lunimals.exe"
        echo "Note: Lunimals installed. Once you start the game, you must press tab until you hear sapi on to get speech." >&2
    ;;
    "Manamon")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://www.vgstorm.com/manamon/manamon_installer.exe"
        wine "${cache}/manamon_installer.exe" /silent
        add_launcher "c:\Program Files\VGStorm.com\Manamon\rpg.exe"
    ;;
    "Manamon 2")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.vgstorm.com/manamon2/manamon2_installer.exe"
        wine "${cache}/manamon2_installer.exe" /silent
        add_launcher "c:\Program Files\VGStorm.com\Manamon 2\rpg.exe"
    ;;
    "Marina Break")
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://www.nyanchangames.com/softs/MbSetupE.exe" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        wine "${cache}/MbSetupE.exe" &
        xdotool sleep 10 key Return
        wineserver -w
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient.dll" -exec cp -v "$cache/nvda2speechd32.dll" "{}" \;
        add_launcher "c:\nyanchangame\MarinaBreak\marinabreak.exe"
    ;;
    "Minecraft")
        export WINEARCH=win64
        export winVer="win7"
        install_wine_bottle
        get_installer "accessibility-plus-extended-1.18-curseforge-v1.9.0.jar" "https://www.curseforge.com/minecraft/mc-mods/accessibility-plus-extended/download"
        get_installer "Mambience-5.2.0+1.19.jar" "https://www.curseforge.com/minecraft/mc-mods/mambience/download"
        get_installer "easy-life-1.18-v1.5.1.jar" "https://www.curseforge.com/minecraft/mc-mods/easy-life/download"
        get_installer "numpad-camera-controls-1.18-v1.5.1.jar" "https://www.curseforge.com/minecraft/mc-mods/numpad-keypad-camera-controls/download"
        download "https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe"
        wine "$cache/python-3.7.9-amd64.exe" /silent
        wine 'c:\windows\py.exe' -m pip install portablemc portablemc-fabric
        # Now just need to install latest fabric, portablemc start --dry fabric
        wine 'c:\users\'"$USER"'\AppData\Local\Programs\Python\Python37\Scripts\portablemc.exe' start --dry fabric:
        # Also need to copy all mods into right place.
        mkdir "$WINEPREFIX/drive_c/users/$USER/AppData/Roaming/.minecraft/mods"
        for x in "accessibility-plus-extended-1.18-curseforge-v1.9.0.jar" "Mambience-5.2.0+1.19.jar" "easy-life-1.18-v1.5.1.jar" "numpad-camera-controls-1.18-v1.5.1.jar" ; do
            cp "$cache/$x" "$WINEPREFIX/drive_c/users/$USER/AppData/Roaming/.minecraft/mods"
        done
        # And of course, add the launcher.
;;
    "Monkey Business")
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk quartz
        download "http://download.dracoent.com/Windows/classic/MBSetup.exe"
        wine "${cache}/MBSetup.exe" /sp- /silent
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet20 by itself so it actually doesn't hang.
        winetricks -q dotnet20
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\Monkey Business\mb.exe"
    ;;
    "MudSplat English")
        a="a"
        f="f"
        i="i"
        n="n"
    ;&
    "MudSplat French")
        # Variables may be already set for English installation, so make sure not to overwrite them.
        a="${a:-j}"
        f="${f:-t}"
        i="${i:-i}"
        n="${n:-s}"
    ;&
    "MudSplat Swedish")
        # Variables may be already set for French installation, so make sure not to overwrite them.
        a="${a:-a}"
        f="${f:-f}"
        i="${i:-i}"
        n="${n:-n}"
        install_wine_bottle
        download "https://www.agarchive.net/games/other/Mudsplat-install.exe"
        wine "${cache}/Mudsplat-install.exe" &
        # Select the language.
        xdotool sleep 10 type --clearmodifiers ${game:9:1} 2> /dev/null
        xdotool sleep 1 key --clearmodifiers Return sleep 1 key alt+${n} sleep 1 key alt+${a} sleep 1 key alt+${n} sleep 1 key space sleep 1 key alt+${n} sleep 1 key alt+${n} sleep 1 key alt+${i} sleep 10 key space sleep 1 key alt+${f} 2> /dev/null
        wineserver -w
        mudsplatLauncher="$(find "$WINEPREFIX/drive_c/Program Files/TiM/MudSplat" -name 'mudsplat-*.exe')"
        mudsplatLauncher="${mudsplatLauncher##*/}"
        add_launcher "c:\Program Files\TiM\MudSplat\\${mudsplatLauncher}"
    ;;
    "Oh Shit")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://samtupy.com/stevend/oh_shit.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/oh_shit.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\oh_shit\OhShit.exe"
    ;;
    "Operation BlackSquare")
        export winVer="win7"
        install_wine_bottle
        download "https://www.iamtalon.me/games/blacksquare.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/blacksquare.zip"
        add_launcher "c:\Program Files\blacksquare\OperationBlackSquare.exe"
    ;;
    "Pacman Talks")
        install_wine_bottle
        download "http://www.gmagames.com/pmt101.exe"
        wine "${cache}/pmt101.exe" /sp- /silent
        add_launcher "c:\Program Files\Pacman Talks\pmt.exe"
    ;;
    "Palace Punch Up")
        install_wine_bottle speechsdk
        download "https://www.agarchive.net/games/blastbay/palace%20punch-up%20setup.exe"
        wine "${cache}/palace punch-up setup.exe" /silent
        add_launcher "c:\Program Files\Palace Punch-up\palace.exe"
    ;;
    "Paladin of the Sky")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.vgstorm.com/cod/pots/paladin_installer.exe"
        wine "${cache}/paladin_installer.exe" /silent
        add_launcher "c:\Program Files\VGStorm.com\Paladin of the Sky\game.exe"
    ;;
    "Park Boss")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.ndadamson.com/downloads/Park%20Boss%201.01%20setup.exe"
        wine "${cache}/Park Boss 1.01 setup.exe" /silent &
        xdotool sleep 10 key --clearmodifiers Return sleep 1 key alt+n sleep 1 key alt+a sleep 1 key alt+i sleep 10 key alt+f 2> /dev/null
        wineserver -w
        add_launcher "c:\Program Files\NASoft\ParkBoss\pbMain.exe"
    ;;
    "Paw Prints")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz speechsdk corefonts
        download "https://www.kaldobsky.com/audiogames/pawprints.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/pawprints" "${cache}/pawprints.zip"
        wine 'c:\Program Files\pawprints\checkup.exe' /verysilent
        add_launcher "c:\Program Files\pawprints\PawPrints.exe"
    ;;
    "Perilous Hearts")
        install_wine_bottle speechsdk
        download "https://www.agarchive.net/games/blastbay/perilous%20hearts%20concept%20demo.exe"
        wine "${cache}/perilous hearts concept demo.exe" /silent
        add_launcher "c:\Program Files\Perilous Hearts Concept Demo\perilous_hearts.exe"
    ;;
    "Pigeon Panic")
        export bottle="l-works"
        install_wine_bottle vb6run dx8vb
        download "http://agarchive.net/games/lworks/pigeon%20panic%20setup.exe"
        wine "${cache}/pigeon panic setup.exe" /silent
        add_launcher "c:\Program Files\Lworks\Pigeon Panic\pigeonPanic.exe"
    ;;
    "Preludeamals")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz speechsdk corefonts
        download "https://www.kaldobsky.com/audiogames/Preludeamals.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/preludeamals" "${cache}/Preludeamals.zip"
        wine 'c:\Program Files\preludeamals\checkup.exe' /verysilent
        add_launcher "c:\Program Files\preludeamals\Preludeamals.exe"
    ;;
    "Psycho Strike")
        install_wine_bottle speechsdk
        download "http://www.vgstorm.com/psycho_strike_installer.exe"
        wine "${cache}/psycho_strike_installer.exe" /silent
        add_launcher "c:\Program Files\VGStorm.com\Psycho Strike\strike.exe"
    ;;
    "Puzzle Divided")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz corefonts
        download "https://www.kaldobsky.com/audiogames/puzzledivided.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/puzzledivided" "${cache}/puzzledivided.zip"
        add_launcher "c:\Program Files\puzzledivided\PuzzleDivided.exe"
    ;;
    "Q9")
        install_wine_bottle
        download "http://www.blastbay.com/q9_english_installer.exe"
        wine "${cache}/q9_english_installer.exe" /silent
        add_launcher "c:\Program Files\Q9 Action Game\q9.exe"
    ;;
    "Revelation")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        install_wine_bottle vb6run dx8vb quartz speechsdk corefonts
        download "https://www.kaldobsky.com/audiogames/revelation.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/revelation" "${cache}/revelation.zip"
        add_launcher "c:\Program Files\revelation\Revelation.exe"
    ;;
    "Rhythm Rage")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://oriolgomez.com/games/rr_en.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/rhythm rage" "${cache}/rr_en.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\rhythm rage\game.exe"
    ;;
    "River Raiders")
        # No more choppy sound under water, woot!
        install_wine_bottle dsound directmusic
        download "https://www.agarchive.net/games/XSight/River%20Raiders%201.3.5.exe"
        wine "$cache/River Raiders 1.3.5.exe" &
        xdotool sleep 10 type y 2> /dev/null
        xdotool sleep 2 type y 2> /dev/null
        xdotool sleep 2 key --clearmodifiers alt+n sleep 2 key alt+n sleep 2 key alt+n sleep 2 key alt+i sleep 10 key alt+f 2> /dev/null
        wineserver -w
        add_launcher "c:\Program Files\River Raiders\raid.exe"
    ;;
    "Road to Rage")
        export WINEARCH=win64
        export winVer="win7"
        install_wine_bottle
        download "https://iamtalon.me/games/rtr_ultimate.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/rtr_ultimate.zip"
        add_launcher "c:\Program Files\rtr_Ultimate\trtr.exe"
    ;;
    "Road to Rage Offline")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://agarchive.net/games/talon/the%20road%20to%20rage%20offline.7z"
        7z x -o"$WINEPREFIX/drive_c/Program Files" "${cache}/the road to rage offline.7z"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\RTR Offline\rtr.exe"
    ;;
    "RS Games")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://rsgames.org/rsdownloads/rsgclient/rsgames-client-setup-2.01.exe" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        wine "${cache}/rsgames-client-setup-2.01.exe" /silent
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\RS Games Client\rsg.exe"
    ;;
    "Run For Your Life")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/rfyl_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/run for your life" "${cache}/rfyl_en.zip"
        add_launcher "c:\Program Files\run for your life\game.exe"
    ;;
    "Sammy Center")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.samtupy.com/games/SCSetup.exe" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        wine "${cache}/SCSetup.exe" /silent
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\Sam Tupy\SammyCenter\SammyCenter.exe"
    ;;
    "Scramble!")
        winetricksSettings="vd=1024x768"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://stevend.net/downloads/scramble_win32.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/scramble_win32.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\scramble_win32\scramble.exe"
    ;;
    "Screaming Strike 2")
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle fakejapanese speechsdk
        download "https://www.nyanchangames.com/softs/screamingStrike2.exe" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        wine "${cache}/screamingStrike2.exe" &
        xdotool sleep 10 key Return
        wineserver -w
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\nyanchangame\Screaming Strike 2\play.exe"
    ;;
    "Sketchbook")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://games.ims-productions.com/SBYW/SBYW.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/scrolling battles" "${cache}/SBYW.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\scrolling battles\SBYW.exe"
    ;;
    "Sequence Storm")
        get_installer "sequence-storm-win64.zip" "https://special-magic-games-llc.itch.io/sequence-storm"
        export WINEARCH=win64
        export winVer="win10"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/sequence-storm" "${cache}/sequence-storm-win64.zip"
        write_sequence_storm_reader
        wget -O "$WINEPREFIX/drive_c/Program Files/sequence-storm/settings.json" "https://stormgames.wolfe.casa/downloads/sequencestorm-settings.json"
        add_launcher "c:\Program Files\sequence-storm\SequenceStorm.exe"
    ;;
    "Shades of Doom 1.2")
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://gmagames.com/sod1208.exe"
        wine "${cache}/sod1208.exe" /sp- /verysilent
        add_launcher "c:\Program Files\Shades of Doom 1.2\sod.exe"
    ;;
    "Shades of Doom")
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://www.gmagames.com/sod20022.exe"
        wine "${cache}/sod20022.exe" /silent
        add_launcher "c:\Program Files\Shades of Doom 2.0\sod.exe"
    ;;
    "Shadow Line")
        export winVer="win7"
        install_wine_bottle
        download "https://www.mm-galabo.com/sr/Download_files_srfv/shadowrine_fullvoice3.171.exe"
        wine "${cache}/shadowrine_fullvoice3.171.exe" /sp- &
        xdotool sleep 30 key --clearmodifiers --delay=75 Return
        xdotool key --clearmodifiers --delay=500 Return
        xdotool key --clearmodifiers --delay=500 Return
        xdotool key --clearmodifiers --delay=500 Return
        xdotool sleep 300 key --clearmodifiers --delay=500 Down
        xdotool key --clearmodifiers --delay=500 space
        xdotool key --clearmodifiers --delay=500 alt+f
        wineserver -w
        add_launcher "c:\Program Files\GalaxyLaboratory\ShadowRine_FullVoice\play_sr.exe"
    ;;
    "Silver Dollar")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://download.dracoent.com/Windows/SilverDollarSetup.exe"
        wine "${cache}/SilverDollarSetup.exe" /silent
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet40 by itself so it actually doesn't hang.
        DISPLAY="" winetricks -q dotnet40
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\Silver Dollar\SilverDollarGui.exe"
    ;;
   "Slender Lost Vision")
        export winVer="win7"
        install_wine_bottle
        download "https://www.iamtalon.me/games/slender.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/slender.zip"
        add_launcher "c:\Program Files\slender\slender.exe"
   ;;
   "Shooter")
        export WINEARCH=win64
        export winVer="win7"
        install_wine_bottle
        download "http://masonasons.me/softs/Shooter.zip" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/shooter" "${cache}/Shooter.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient64.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\shooter\shooter.exe"
   ;;
    "Sonic the Hedgehog")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://www.agarchive.net/games/jeqoconGames/sonic%20the%20hedgehog.7z"
        7z e -o"$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog" "${cache}/sonic the hedgehog.7z"
        cp -v "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.wav "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.ogg "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.mp3 "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/sfx/
        cp -v "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.wav "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.ogg "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.mp3 "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/music/
        cp -v "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.wav "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.ogg "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/*.mp3 "$WINEPREFIX/drive_c/Program Files/Sonic the Hedgehog"/angles/
        add_launcher "c:\Program Files\Sonic the Hedgehog\sth.exe"
    ;;
   "Sonic Zoom")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://wwwx.cs.unc.edu/Research/assist/et/projects/SonicZoom/soniczoom11.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/soniczoom11.zip"
        add_launcher "c:\Program Files\Sonic Zoom\SonicZoom.exe"
   ;;
   "Space Defender")
        export WINEARCH="win64"
        export winVer="win7"
        install_wine_bottle
        download "http://tunmi13.ddns.net/projects/space_defender.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient64.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/" "${cache}/space_defender.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient64.dll' -exec cp -v "${cache}/nvdaControllerClient64.dll" "{}" \;
        add_launcher "c:\Program Files\space_defender\sdefender.exe"
   ;;
    "Super Dogs Bone Hunt")
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://www.pcs-games.net/SBH11.exe"
        wine "${cache}/SBH11.exe" /sp- /silent
        add_launcher "c:\Program Files\SuperDog's Bone Hunt\sbh.exe"
    ;;
    "Super Egg Hunt")
        export bottle="l-works"
        install_wine_bottle
        download "http://files.l-works.net/superegghuntsetup.exe"
        wine "${cache}/superegghuntsetup.exe" /silent
        add_launcher "c:\Program Files\Lworks\super egg hunt\superegghunt.exe"
    ;;
    "Super Liam")
        export bottle="l-works"
        install_wine_bottle vb6run dx8vb
        download "http://files.l-works.net/superliamsetup.exe"
        wine "${cache}/superliamsetup.exe" /silent
        add_launcher "c:\Program Files\lWorks\Super Liam\sl.exe"
    ;;
    "Super Mario Bros")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "https://www.agarchive.net/games/jeqoconGames/super%20mario%20bros.7z"
        7z e -o"$WINEPREFIX/drive_c/Program Files/Super Mario Bros" "${cache}/super mario bros.7z"
        cp -v "$WINEPREFIX/drive_c/Program Files/Super Mario Bros"/*.wav "$WINEPREFIX/drive_c/Program Files/Super Mario Bros"/*.ogg "$WINEPREFIX/drive_c/Program Files/Super Mario Bros"/mario/
        add_launcher "c:\Program Files\Super Mario Bros\Mario.exe"
    ;;
    "Survive the Wild")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.samtupy.com/games/stw.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/Survive the Wild" "${cache}/stw.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\Survive the Wild\stw.exe"
    ;;
    "Swamp")
        export bottle="aprone"
        export winVer="win7"
        export winetricksSettings="vd=1024x768"
        dialog --backtitle "Audiogame manager" --yesno "If you do not have a full 32 bit gstreamer installation, the Swamp music can cause stuttering and crashes. Would you like to remove the music directory after installation?" -1 -1 --stdout
        deleteMusic=$?
        install_wine_bottle dx8vb quartz corefonts vb6run speechsdk
        download "https://www.kaldobsky.com/audiogames/Swamp.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/swamp" "${cache}/Swamp.zip"
        # make sure the latest version is installed.
        #if wget -O "${cache}/SwampPatch.zip" "https://www.kaldobsky.com/audiogames/SwampPatch.zip" ; then
            #nzip -o -d "$WINEPREFIX/drive_c/Program Files/swamp" "${cache}/SwampPatch.zip"
        #fi
        wine 'c:\Program Files\swamp\checkup.exe' /verysilent
        #wine cmd.exe /c 'cd /d c:\Program Files\swamp && Windows32bit.bat'
        # Delete music if requested.
        if [[ $deleteMusic -eq 0 ]]; then
            rm -frv "$WINEPREFIX/drive_c/Program Files/swamp/sounds/Music/"
        fi
        add_launcher "c:\Program Files\swamp\Swamp.exe"
    ;;
    "Technoshock")
        install_wine_bottle
        download "http://tiflocomp.ru/download/games/technoshock_140b_en.zip" "http://tiflocomp.ru/download/games/technoshock140b_en_update.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/technoshock_140b_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/technoshock_140b_en_update.zip"
        wine "$WINEPREFIX/drive_c/Program Files/setup_eng.exe" /silent
        wineserver -w
        wine "$WINEPREFIX/drive_c/Program Files/setup_eng_update_pack.exe" /silent
        add_launcher "c:\Program Files\Tiflocomp Games\Technoshock\ts.exe"
    ;;
    "Ten Pin Alley")
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk quartz
        download "http://download.dracoent.com/Windows/classic/TPAXPSetup.exe"
        wine "${cache}/TPAXPSetup.exe" /sp- /silent
        #winetricks -q msdxmocx # I think having this installed first breaks things.
        # warning warning warning: Do not change location, or installer will not function.
        # FIXME: Hacky, but it works. Install dotnet20 by itself so it actually doesn't hang.
        winetricks -q dotnet20
        wineserver -k # Damn you, dotnet.
        add_launcher "c:\Program Files\Draconis Entertainment\Ten Pin Alley\tpa.exe"
    ;;
    "The Blind Swordsman")
        install_wine_bottle
        download "https://www.agarchive.net/games/other/the%20blind%20swordsmanPC.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/the blind swordsmanPC.zip"
        add_launcher "c:\Program Files\TheBlindSwordsman.exe"
    ;;
    "The Gate")
        export winVer="win7"
        install_wine_bottle
        download "http://www.vgstorm.com/the_gate_installer.exe"
        wine "${cache}/the_gate_installer.exe" /silent
        add_launcher "c:\Program Files\VGStorm.com\The Gate\gate.exe"
    ;;
    "The Great Toy Robbery")
        export bottle="l-works"
        export winVer="win7"
        install_wine_bottle  speechsdk
        download "http://files.l-works.net/tgtrsetup_2.04.exe" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        wine "${cache}/tgtrsetup_2.04.exe" /silent
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\Lworks\The Great Toy Robbery\tgtr.exe"
    ;;
    "The Vale")
        get_installer "the-vale-win.zip" "https://falling-squirrel.itch.io/the-vale"
        export WINEARCH=win64
        export winVer="win8"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/the-vale" "${cache}/the-vale-win.zip"
        # Weird work around to get keyboard working.
        winetricks -q usetakefocus=y
        winetricks -q usetakefocus=n
        add_launcher "c:\Program Files\the-vale\TheVale.exe"
    ;;
    "Thief")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle
        download "http://oriolgomez.com/games/thief_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/thief" "${cache}/thief_en.zip"
        add_launcher "c:\Program Files\thief\game.exe"
    ;;
    "Top Speed 2")
        install_wine_bottle
        download "http://users.pandora.be/playinginthedark/downloads/Tspeed_2.00.exe"
        wine "${cache}/Tspeed_2.00.exe" /silent
        add_launcher "c:\Program Files\Playing in the dark\Top Speed 2\TopSpeed.exe"
    ;;
    "Tomb Hunter")
        install_wine_bottle speechsdk
        download "http://masonasons.me/softs/th_freeware_password_is_tombhunter.7z"
        7z e -o"$WINEPREFIX/drive_c/Program Files/Tomb Hunter" "${cache}/th_freeware_password_is_tombhunter.7z" -ptombhunter
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\Tomb Hunter\th.exe"
    ;;
    "Top Speed 3")
        install_wine_bottle directplay
        download "https://github.com/PlayingintheDark/TopSpeed/releases/download/h/Tspeed_3.0.3.exe"
        wine "${cache}/Tspeed_3.0.3.exe" /silent
        add_launcher "c:\Program Files\Playing in the dark\Top Speed 3\TopSpeed.exe"
    ;;
    "Traders of Known Space")
        install_wine_bottle
        download "http://www.valiantgalaxy.com/tks/tradersOfKnownSpace0.0.0.5Installer.exe"
        wine "${cache}/tradersOfKnownSpace0.0.0.5Installer.exe" /silent
        add_launcher "c:\Program Files\VGA\TKS\tradersOfKnownSpace.exe"
    ;;
    "Three D velocity")
        export winVer="win10"
        install_wine_bottle  speechsdk
        # Dotnet is evil. That is all.
        LC_ALL=C winetricks -q dotnet472
        wineserver -k # Ha ha ha.
        download "https://github.com/munawarb/Three-D-Velocity-Binaries/archive/master.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/master.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\Three-D-Velocity-Binaries-master\tdv.exe"
    ;;
    "Triple Triad")
        export bottle="aprone"
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk
        download "https://www.kaldobsky.com/audiogames/tripletriad.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/Triple Triad" "${cache}/tripletriad.zip"
        wine "c:\Program Files\Triple Triad\Checkup.exe" /verysilent
        add_launcher "c:\Program Files\Triple Triad\TripleTriad.exe"
    ;;
    "Troopanum2")
        install_wine_bottle vb6run dx8vb
        # FIXME: Hacky, but it works. Install dotnet35 by itself so it actually doesn't hang.
        DISPLAY="" winetricks -q dotnet35
        wineserver -k # Damn you, dotnet.
        download "https://www.agarchive.net/games/bsc/Troopanum2Setup.exe" "https://www.agarchive.net/games/bsc/BSC%20unlock%20code%20generator.7z"
        wine "${cache}/Troopanum2Setup.exe" /silent &
        xdotool sleep 10 key --clearmodifiers alt+y 2> /dev/null
        sleep 30 && wineserver -k # Sometimes the installer finishes but the wineserver has more processes that don't exit, so we can't depend on wineserver -w.
        mkdir -p "$WINEPREFIX/drive_c/Program Files/bsc-key-generator"
        7z e -o"$WINEPREFIX/drive_c/Program Files/bsc-key-generator" "${cache}/BSC unlock code generator.7z"
        echo "$USER"$'\n'"$(hostname)"$'\n'"none"$'\n'"US" > "$WINEPREFIX/drive_c/Program Files/Troopanum 2.0/config.dat"
        command -v unix2dos &> /dev/null && unix2dos "$WINEPREFIX/drive_c/Program Files/Troopanum 2.0/config.dat" || echo "Warning! You do not have unix2dos or dos2unix installed, Troopanum 2.0 may not work!"
        if command -v xclip &> /dev/null && command -v xdotool &> /dev/null ; then
            wine "c:\Program Files\Troopanum 2.0\register.exe" &
            xdotool sleep 10 key Return sleep 2 key Return sleep 2 key Alt+n
            sleep 1
            regcode="$(xclip -selection clipboard -o)"
            xdotool sleep 1 key Shift+Tab sleep 1 key Return
            # FIXME: Kind of hacky, but let's make sure it actually exitted.
            sleep 5
            wineserver -k
            wine "c:\Program Files\bsc-key-generator\BlindsoftwareUnlockCodeGenerator.exe" &
            xdotool sleep 10 key Return sleep 2 type t
            xdotool sleep 1 type t
            xdotool sleep 1 key Tab sleep 1 type $regcode
            xdotool sleep 1 key Tab sleep 1 key Return
            sleep 2
            regcode="$(xclip -selection clipboard -o)" # Might as well reuse the variable.
                        # FIXME: Kind of hacky, but let's make sure it actually exitted since I can't find a good way to exit this program.
            sleep 5
            wineserver -k
                     wine "c:\Program Files\Troopanum 2.0\register.exe" &
            echo "$regcode" | xclip -selection clipboard
            xdotool sleep 10 key Return sleep 2 key Shift+Tab sleep 1 key Shift+Tab sleep 1 key Return
            # FIXME: Kind of hacky, but let's make sure it actually exitted.
            sleep 5
            wineserver -k
        else
            echo "Warning: You need xclip and xdotool for this installer to finish the registration process, however, you don't have them. Either remove the program, install the stated tools, and reinstall it, or go on your own. Caveat Emptor, go to jail, do not pass go, do not collect audiogame rewards and all that stuff."
        fi
        add_launcher "c:\Program Files\Troopanum 2.0\troop.exe"
    ;;
    "Tube Sim")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://www.ndadamson.com/downloads/TubeSim1_1_Install.exe"
        wine "${cache}/TubeSim1_1_Install.exe" /silent &
        xdotool sleep 10 key --clearmodifiers Return sleep 1 key alt+n sleep 1 key alt+a sleep 1 key alt+i sleep 10 key alt+f 2> /dev/null
        wineserver -w
        add_launcher "c:\Program Files\NASoft\TubeSim\tsMain.exe"
    ;;
    "Undead Assault")
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://undead-assault.com/static/files/public/undead_assault.zip" "https://stormgames.wolfe.casa/downloads/nvdaControllerClient32.dll"
        unzip -d "$WINEPREFIX/drive_c/Program Files/undead_assault" "${cache}/undead_assault.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient32.dll' -exec cp -v "${cache}/nvdaControllerClient32.dll" "{}" \;
        add_launcher "c:\Program Files\undead_assault\Undead Assault.exe"
    ;;
    "Villains From Beyond")
        export bottle="oriol-gomez"
        export winVer="win7"
        install_wine_bottle speechsdk
        download "http://oriolgomez.com/games/villains_en.zip"
        unzip -d "$WINEPREFIX/drive_c/Program Files/villains from beyond" "${cache}/villains_en.zip"
        find "${WINEPREFIX}" -type f -name "nvdaControllerClient32.dll" -exec rm -fv "{}" \;
        add_launcher "c:\Program Files\villains from beyond\game.exe"
    ;;
    "VIP Mud")
        export winVer="win7"
        install_wine_bottle vb6run dx8vb speechsdk
        download "http://gmagames.com/vipmud20016.exe"
        wine "${cache}/vipmud20016.exe" /silent
        mkdir -p "${HOME}/.local/wine/vip-mud/drive_c/users/${USER}/Documents/VIP Mud"
        add_launcher "c:\Program Files\VIPMud 2.0\vipmud2.exe"
        echo
        echo "When you first launch VIP Mud You will be presented with several inaccessible dialogs."
        echo "To bypass these dialogs, press alt+o, then press enter until speech starts."
        echo "To be sure that each new dialog has had time to complete, wait a second between each press of enter."
        echo "In each subsequent launch, you will need to hit enter a couple times before it starts speaking."
    ;;
    "Warsim")
        get_installer "Warsim Full Game.zip" "https://huw2k8.itch.io/warsim"
        export winVer="win7"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files/Warsim/" "${cache}/Warsim Full Game.zip"
        add_launcher "c:\Program Files\Warsim\Warsim.exe"
    ;;
    "Wolf Games Launcher")
        download "https://sightlesswolf.com/wg-release.zip" "https://github.com/RastislavKish/nvda2speechd/releases/download/v0.1/nvda2speechd64.dll"
        export WINEARCH=win64
        export winVer="win7"
        install_wine_bottle
        unzip -d "$WINEPREFIX/drive_c/Program Files" "${cache}/wg-release.zip"
        find "${WINEPREFIX}" -type f -name 'nvdaControllerClient64.dll' -exec cp -v "${cache}/nvda2speechd64.dll" "{}" \;
        add_launcher "c:\Program Files\wg-release\WolfGames.exe"
    ;;
    "World of War")
        export bottle="nyanchan"
        export winVer="win7"
        install_wine_bottle
        download "https://www.agarchive.net/games/nyanchan/world%20of%20war%20English.7z"
        7z x -o"$WINEPREFIX/drive_c/nyanchangame" "${cache}/world of war English.7z"
        add_launcher "c:\nyanchangame\world of war English\world of war.exe"
    ;;
    "Donate")
        open_url "https://ko-fi.com/stormux"
    ;;
    "Become a Patron")
        open_url "https://2mb.games/product/2mb-patron/"
    ;;
    *)
        [[ -n "${game}" ]] && echo "Game \"${game}\" not found."
        exit 1
    ;;
esac

exit 0
